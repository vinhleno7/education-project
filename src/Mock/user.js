const user = [
  {
    id: 1,
    name: "Lê Vinh",
    image:
      "https://scontent.fsgn13-2.fna.fbcdn.net/v/t39.30808-6/274240760_1260036157738588_7703011298435152142_n.jpg?_nc_cat=108&ccb=1-5&_nc_sid=09cbfe&_nc_ohc=9ZYVRhOnnSAAX_ODMWz&_nc_ht=scontent.fsgn13-2.fna&oh=00_AT9D6rZLpGkx3Egm1iFDsoYcGT9gN2rkEyH0X5qWIIJClw&oe=6246B196",
    isMe: true,
    messages: [
      {
        userId: 2,
        message: [
          {
            _id: 1,
            mess: "Hello my love"
          },
          {
            _id: 2,
            mess: "I love you so much"
          },
          {
            _id: 3,
            mess: "I am going to marry you"
          }
        ]
      }
    ],
    isActive: true
  },
  {
    id: 2,
    name: "Vân Anh",
    image:
      "https://scontent.fsgn8-1.fna.fbcdn.net/v/t1.6435-9/91393919_656745315151426_534799054136147968_n.jpg?_nc_cat=111&ccb=1-5&_nc_sid=174925&_nc_ohc=hv3N-1eCRyIAX8SVw26&_nc_ht=scontent.fsgn8-1.fna&oh=00_AT-vl2EhiVObu7JrO79EcMgjE7fDeNRm2qFjqzcMiaErig&oe=626775E9",
    messages: [
      {
        userID: 1,
        message: [
          {
            _id: 1,
            mess: "I love you"
          },
          {
            _id: 2,
            mess: "I know that, hey...."
          },
          {
            _id: 3,
            mess: "Yes, I do"
          }
        ]
      }
    ],
    isActive: true
  },
  {
    id: 3,
    name: "Cristiano Ronaldo ",
    image: "https://cauthu.com.vn/wp-content/uploads/2021/10/3-4.png"
  },
  {
    id: 4,
    name: "Messi",
    image: "https://znews-photo.zadn.vn/w660/Uploaded/natmzz/2022_03_10/mk.JPG"
  }
];

export default user;
