import imgdesign from "../assets/img/bg/img_design.jpg";
import imggeometry from "../assets/img/bg/img_geometry.jpg";
import imglearnlanguages from "../assets/img/bg/img_learnlanguage.jpg";
import imgbreakfast from "../assets/img/bg/img_breakfast.jpg";
import imgreachout from "../assets/img/bg/img_reachout.jpg";
import imgcode from "../assets/img/bg/img_code.jpg";
import imgbookclub from "../assets/img/bg/img_bookclub.jpg";
import imggraducation from "../assets/img/bg/img_graduation.jpg";
import imgbacktoschool from "../assets/img/bg/img_backtoschool.jpg";
import avatardefault from "../assets/img/avatar/avatar-default.png";

import bannerSubject from "../assets/img/subjects/banner2.jpg";

export const images = {
  imgdesign: imgdesign,
  imggeometry: imggeometry,
  imglearnlanguages: imglearnlanguages,
  imgbreakfast: imgbreakfast,
  imgreachout: imgreachout,
  imgcode: imgcode,
  imgbookclub: imgbookclub,
  imggraducation: imggraducation,
  imgbacktoschool: imgbacktoschool
};

//random bg-image
export const dataListSubject = [
  {
    id: 1,
    name: "Thực hành Hướng Đối Tượng",
    image: bannerSubject,
    classSubjects: [
      {
        id_subject: 1,
        nameclass: "Thực hành Hướng Đối Tượng(Nhóm 2)",
        nameteacher: "Bang Bui Nhat",
        avatar: avatardefault,
        image: images.imggeometry
      },
      {
        id_subject: 2,
        nameclass: "Thực hành Hướng Đối Tượng(Nhóm 1)",
        nameteacher: "Bang Bui Nhat",
        avatar: avatardefault,
        image: images.imgdesign
      }
    ]
  },
  {
    id: 2,
    name: "Hướng Đối Tượng",
    image: bannerSubject,
    classSubjects: [
      {
        id_subject: 3,
        nameclass: "Hướng Đối Tượng(Ca 1)",
        nameteacher: "Giáo Viên 1",
        avatar: avatardefault,
        image: images.imglearnlanguages
      },
      {
        id_subject: 4,
        nameclass: "Hướng Đối Tượng(Ca 2)",
        nameteacher: "Giáo Viên 1",
        avatar: avatardefault,
        image: images.imgbreakfast
      }
    ]
  },
  {
    id: 3,
    name: "Thực hành Java",
    image: bannerSubject,
    classSubjects: [
      {
        id_subject: 5,
        nameclass: "Thực hành Java(Ca 3)",
        nameteacher: "Luong An Vinh",
        avatar: avatardefault,
        image: images.imgreachout
      },
      {
        id_subject: 6,
        nameclass: "Thực hành Java(Ca 4)",
        nameteacher: "Luong An Vinh",
        avatar: avatardefault,
        image: images.imgcode
      },
      {
        id_subject: 7,
        nameclass: "Thực hành Java(Ca 5)",
        nameteacher: "Luong An Vinh",
        avatar: avatardefault,
        image: images.imgbookclub
      }
    ]
  },
  {
    id: 4,
    name: "Thực hành ReactJs",
    image: bannerSubject,
    classSubjects: [
      {
        id_subject: 8,
        nameclass: "Thực hành ReactJs",
        nameteacher: "Hau Nguyen",
        avatar: avatardefault,
        image: images.imggraducation
      }
    ]
  },
  {
    id: 5,
    name: "Thực hành MySQL",
    image: bannerSubject,
    classSubjects: [
      {
        id_subject: 5,
        nameclass: "Thực hành MySQL",
        nameteacher: "Nguyen Ngoc Lam",
        avatar: avatardefault,
        image: images.imgbacktoschool
      }
    ]
  },
  {
    id: 6,
    name: "Thực tập TN 2022",
    image: bannerSubject,
    classSubjects: [
      {
        id_subject: 6,
        nameclass: "Thực tập",
        nameteacher: "Thầy Bằng",
        avatar: avatardefault,
        image: images.imgbacktoschool
      }
    ]
  }
];
const dataClassList = [
  {
    id_subject: 3,
    nameclass: "Thực hành Java(Ca 3)",
    nameteacher: "Luong An Vinh",
    avatar: avatardefault,
    image: images.imgreachout
  },
  {
    id_subject: 3,
    nameclass: "Thực hành Java(Ca 4)",
    nameteacher: "Luong An Vinh",
    avatar: avatardefault,
    image: images.imgcode
  },
  {
    id_subject: 3,
    nameclass: "Thực hành Java(Ca 5)",
    nameteacher: "Luong An Vinh",
    avatar: avatardefault,
    image: images.imgbookclub
  },
  {
    id_subject: 4,
    nameclass: "Thực hành ReactJs",
    nameteacher: "Hau Nguyen",
    avatar: avatardefault,
    image: images.imggraducation
  },
  {
    id_subject: 5,
    nameclass: "Thực hành MySQL",
    nameteacher: "Nguyen Ngoc Lam",
    avatar: avatardefault,
    image: images.imgbacktoschool
  }
];

export default dataClassList;
