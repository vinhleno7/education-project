import avatardefault from "../assets/img/avatar/avatar-default.png";
const listMember = [
  {
    id: 1,
    name: "Lê Minh Tú",
    avatar: avatardefault,
    classSubjects: [
      {
        id_subject: 1,
        id_group: 1
      },
      {
        id_subject: 2,
        id_group: 2
      }
    ]
  },
  {
    id: 2,
    name: "Võ Ngọc Bội",
    avatar: avatardefault,
    classSubjects: [
      {
        id_subject: 1,
        id_group: 1
      },
      {
        id_subject: 2,
        id_group: 2
      }
    ]
  },
  {
    id: 3,
    name: "Nguyễn Hữu Lê Vinh",
    avatar: avatardefault,
    classSubjects: [
      {
        id_subject: 1,
        id_group: 1
      },
      {
        id_subject: 2,
        id_group: 2
      }
    ]
  },
  { id: 4, name: "Lê Thái Thông", avatar: avatardefault },

  { id: 5, name: "Lạc Hồng Duy", avatar: avatardefault },
  { id: 6, name: "Đặng Phước Lộc", avatar: avatardefault },
  { id: 7, name: "Ao Nhật Tân", avatar: avatardefault }
];

export default listMember;
