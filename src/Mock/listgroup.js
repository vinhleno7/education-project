import avatardefault from "../assets/img/avatar/avatar-default.png";

import vinh from "../assets/img/user/levinh.jpg";
import tu from "../assets/img/user/minhtu.jpg";
import duy from "../assets/img/user/duylac.jpg";
import thong from "../assets/img/user/thong.png";
const group = [
  {
    id: 1,
    name: "Team #1",
    avatar: [
      {
        name: "Nguyễn Hữu Lê Vinh",
        isLeader: true,
        avatar: vinh
      },
      {
        name: "Lê Minh Tú",
        avatar: tu
      },
      {
        name: "Lạc Hồng Duy",
        avatar: duy
      },
      {
        name: "Thái Thông",
        avatar: thong
      }
    ],
    isActive: true
  },
  {
    id: 2,
    name: "Team #2",
    avatar: [
      avatardefault,
      avatardefault,
      avatardefault,
      avatardefault,
      avatardefault,
      avatardefault
    ]
  },
  {
    id: 3,
    name: "Team #3",
    avatar: [avatardefault, avatardefault]
  },
  {
    id: 4,
    name: "Team #4",
    avatar: [
      avatardefault,
      avatardefault,
      avatardefault,
      avatardefault,
      avatardefault
    ]
  },
  {
    id: 5,
    name: "Team #Dev",
    avatar: [
      avatardefault,
      avatardefault,
      avatardefault,
      avatardefault,
      avatardefault,
      avatardefault,
      avatardefault,
      avatardefault
    ]
  },
  {
    id: 6,
    name: "Team #Design",
    avatar: [avatardefault, avatardefault]
  },
  {
    id: 7,
    name: "Team #Tester",
    avatar: [avatardefault, avatardefault, avatardefault]
  }
];
export default group;
