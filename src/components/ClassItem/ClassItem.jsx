import React from "react";
import classList from "../../Mock/classItem";

function ClassItem() {
  const render = classList.map((item, index) => {
    return (
      <li key={index}>
        <img style={{ width: "50%" }} src={item.image} alt="" />
        {item.name}'
      </li>
    );
  });
  return (
    <div>
      Render các Class Item
      <ul className="LV">{render}</ul>
    </div>
  );
}

export default ClassItem;
