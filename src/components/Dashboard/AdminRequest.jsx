import React, { useEffect, useState } from "react";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import Paper from "@mui/material/Paper";
import axios from "axios";
import { BASE_URL } from "../../constants";
import { IconButton } from "@mui/material";
import DeleteIcon from "@mui/icons-material/Delete";
import AddCircleOutlineIcon from "@mui/icons-material/AddCircleOutline";
import EditIcon from "@mui/icons-material/Edit";
import { useSelector, useDispatch } from "react-redux";

const AdminRequest = () => {
  const [data, setData] = useState([]);
  const { auth } = useSelector((state) => state);
  useEffect(() => {
    const config = {
      headers: {
        "Content-Type": "application/json",
        Authorization: auth.token,
      },
    };
    const fetchData = async () => {
      try {
        const res = await axios.get(
          `${BASE_URL}/api/requestChangeGroup/getAll`,
          config
        );
        setData(res.data);
        console.log(res.data);
      } catch (error) {
        console.error(error);
      }
    };
    fetchData();
  }, []);
  const handleStatusChange = (item) => {
    console.log("item", item);
    let result = "";
    if (item === "yes") {
      result = "Đồng ý";
    } else {
      result = "Từ chối";
    }
    return result;
  };
  return (
    <TableContainer component={Paper}>
      <Table sx={{ minWidth: 650 }} aria-label="simple table">
        <TableHead>
          <TableRow>
            <TableCell>Tiêu đề</TableCell>
            <TableCell align="left">Nội dung</TableCell>
            <TableCell align="left">Nhóm hiện tại</TableCell>
            <TableCell align="left">Nhóm muốn chuyển</TableCell>
            <TableCell align="left">Tên sinh viên</TableCell>
            <TableCell align="left">Tên giảng viên</TableCell>
            <TableCell align="left">Trạng thái</TableCell>
            <TableCell align="left">Hành động</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {data.map((item, i) => (
            <TableRow
              key={i}
              sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
            >
              <TableCell component="th" scope="row">
                {item.title}
              </TableCell>
              <TableCell align="left">{item.content}</TableCell>
              <TableCell align="left">{item.group?.name}</TableCell>
              <TableCell align="left">{item.groupWantChange?.name}</TableCell>
              <TableCell align="left">{item.student.fullName}</TableCell>
              <TableCell align="left">{item.teacher.fullName}</TableCell>
              <TableCell align="left">
                {!item.status ? "Chưa duyệt" : handleStatusChange(item.status)}
              </TableCell>
              <TableCell align="left">
                <div>
                  <IconButton aria-label="add" size="large">
                    <AddCircleOutlineIcon />
                  </IconButton>
                  <IconButton aria-label="delete" size="large">
                    <DeleteIcon />
                  </IconButton>
                  <IconButton aria-label="edit" size="large">
                    <EditIcon />
                  </IconButton>
                </div>
              </TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
};

export default AdminRequest;
