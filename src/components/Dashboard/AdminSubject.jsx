import React, { useEffect, useState } from "react";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import Paper from "@mui/material/Paper";
import axios from "axios";
import { BASE_URL } from "../../constants";
import {
  Button,
  Dialog,
  DialogActions,
  DialogTitle,
  IconButton,
  Modal,
  Typography,
} from "@mui/material";
import DeleteIcon from "@mui/icons-material/Delete";
import AddCircleOutlineIcon from "@mui/icons-material/AddCircleOutline";
import EditIcon from "@mui/icons-material/Edit";
import { Box } from "@mui/system";
import { Field, Form, Formik } from "formik";
import { useSelector } from "react-redux";
import * as Yup from "yup";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
const style = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 600,
  bgcolor: "background.paper",
  border: "2px solid #000",
  boxShadow: 24,
  p: 4,
};
const AdminSubject = () => {
  const [data, setData] = useState([]);
  const { auth } = useSelector((state) => state);
  const [getAllMajor, setGetAllMajor] = useState([]);

  //create
  const [openModelCreate, setOpenModelCreate] = React.useState(false);
  const handleOpenCreate = () => setOpenModelCreate(true);
  const handleCloseCreate = () => setOpenModelCreate(false);
  //delete
  const [openDialogDelete, setOpenDialogDelete] = React.useState(false);
  const handleOpenDelete = () => setOpenDialogDelete(true);
  const handleCloseDelete = () => setOpenDialogDelete(false);
  const [idDel, setIdDel] = useState("");
  //update
  const [openModelUpdate, setOpenModelUpdate] = React.useState(false);
  const handleOpenUpdate = () => setOpenModelUpdate(true);
  const handleCloseUpdate = () => setOpenModelUpdate(false);
  const [idUpdate, setIdUpdate] = useState("");
  const handleClickOpenUpdate = (id, item) => {
    setIdUpdate(id);
    setIdUpdate(item);
    handleOpenUpdate();
  };
  const validUpdate = Yup.object({
    name: Yup.string().required("This is required!"),
    note: Yup.string().required("This is required!"),
  });
  useEffect(() => {
    const config = {
      headers: {
        "Content-Type": "application/json",
        accept: "application/json",
      },
    };
    const fetchData = async () => {
      try {
        const res = await axios.get(
          `${BASE_URL}/api/subject/getAllSubject`,
          config
        );
        setData(res.data);
      } catch (error) {
        console.error(error);
      }
    };
    fetchData();
  }, []);
  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await axios.get(`${BASE_URL}/api/major/getAll`, {
          headers: { Authorization: auth.token },
        });
        setGetAllMajor(response.data.majors);
      } catch (error) {
        console.error(error);
      }
    };
    fetchData();
  }, []);
  const valid = Yup.object({
    majorid: Yup.string().required("This is required!"),
    name: Yup.string().required("No name provided."),
    schoolYear: Yup.string().required("No name provided."),
    note: Yup.string().required("No name provided."),
  });
  const handleSubmit = async (values) => {
    const majorid = values.majorid;
    const name = values.name;
    const schoolYear = values.schoolYear;
    const note = values.note;
    let data = {
      name: name,
      majorID: majorid,
      schoolYear: schoolYear,
      note: note,
    };
    console.log("data", data);

    const config = {
      headers: {
        "Content-Type": "application/json",
        Authorization: auth.token,
      },
    };
    axios
      .post(`${BASE_URL}/api/subject/insert`, data, config)
      .then((result) => {
        toast.success(result.data.msg);
        handleCloseCreate();
        window.location.reload();
      })
      .catch((err) => {
        toast.error(err);
        console.log("err", err);
      });
  };
  const handleClickOpenDelete = (id) => {
    setIdDel(id);
    handleOpenDelete();
  };
  const handleClickDeleteId = (idDel) => {
    console.log("iddel", idDel);
    const fetchData = async () => {
      try {
        const res = await axios.delete(
          `${BASE_URL}/api/subject/delete/${idDel}`
        );
        toast.success(res);
        handleCloseDelete();
        window.location.reload();
      } catch (error) {
        toast.error(error);
      }
    };
    fetchData();
  };
  const handleSubmitUpdate = async (values) => {
    const name = values.name;
    const note = values.note;
    let data = {
      name: name,
      note: note,
    };
    axios
      .put(`${BASE_URL}/api/subject/update/${idUpdate._id}`, data)
      .then((result) => {
        toast.success(result.data);
        handleCloseUpdate();
        window.location.reload();
      })
      .catch((err) => {
        toast.error(err);
      });
  };

  return (
    <>
      <div style={{ marginBottom: "20px" }}>
        <Button
          variant="outlined"
          size="large"
          startIcon={<AddCircleOutlineIcon />}
          onClick={handleOpenCreate}
        >
          Create
        </Button>
      </div>
      <TableContainer component={Paper}>
        <Table sx={{ minWidth: 650 }} aria-label="simple table">
          <TableHead>
            <TableRow>
              <TableCell>Tên Môn Học</TableCell>
              <TableCell align="left">Năm Học</TableCell>
              <TableCell align="left">Ghi chú</TableCell>
              <TableCell align="left">Hành động</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {data.map((item, i) => (
              <TableRow
                key={i}
                sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
              >
                <TableCell component="th" scope="row">
                  {item.name}
                </TableCell>
                <TableCell align="left">{item.schoolYear}</TableCell>
                <TableCell align="left">{item.note}</TableCell>
                <TableCell align="left">
                  <div>
                    <IconButton
                      aria-label="delete"
                      size="large"
                      onClick={() => handleClickOpenDelete(item._id)}
                    >
                      <DeleteIcon />
                    </IconButton>
                    <IconButton
                      aria-label="edit"
                      size="large"
                      onClick={() => handleClickOpenUpdate(item._id, item)}
                    >
                      <EditIcon />
                    </IconButton>
                  </div>
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
      <Modal
        open={openModelCreate}
        onClose={handleCloseCreate}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={style}>
          <Typography id="modal-modal-title" variant="h6" component="h2">
            Thêm môn học
          </Typography>
          <Formik
            initialValues={{
              name: "",
              majorid: "",
              schoolYear: "",
              note: "",
            }}
            validationSchema={valid}
            onSubmit={handleSubmit}
          >
            <Form className="add-form">
              <div className="add-subform">
                <label htmlFor="className">Tên môn học</label>
                <Field id="name" name="name" placeholder="ReactJS" />
              </div>
              <div className="add-subform">
                <label htmlFor="className">Tên Khoa</label>
                <Field as="select" name="majorid">
                  {getAllMajor.map((item, index) => (
                    <option value={item._id} key={index}>
                      {item.name}
                    </option>
                  ))}
                </Field>
              </div>
              <div className="add-subform">
                <label htmlFor="className">Năm học</label>
                <Field
                  id="schoolYear"
                  name="schoolYear"
                  placeholder="2021-2022"
                />
              </div>
              <div className="add-subform">
                <label htmlFor="className">Ghi chú</label>
                <Field id="note" name="note" placeholder="Ghi chú" />
              </div>

              <div className="add-action">
                <Button variant="contained" size="large" type="submit">
                  Thêm mới
                </Button>
                <Button
                  variant="contained"
                  size="large"
                  color="error"
                  onClick={handleCloseCreate}
                >
                  Hủy
                </Button>
              </div>
            </Form>
          </Formik>
        </Box>
      </Modal>
      <Dialog
        open={openDialogDelete}
        onClose={handleCloseDelete}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">
          {"Bạn có chắc chắn xóa môn học này"}
        </DialogTitle>
        <DialogActions>
          <DialogActions>
            <Button onClick={handleCloseDelete}>Hủy</Button>
            <Button
              variant="contained"
              color="error"
              onClick={() => handleClickDeleteId(idDel)}
              autoFocus
            >
              Xóa
            </Button>
          </DialogActions>
        </DialogActions>
      </Dialog>
      <Modal
        open={openModelUpdate}
        onClose={handleCloseUpdate}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={style}>
          <Typography
            id="modal-modal-title"
            variant="h6"
            component="h2"
            sx={{
              textAlign: "center",
              fontSize: "2rem",
              color: "rgb(241, 110, 52)",
            }}
          >
            CẬP NHẬT MÔN HỌC
          </Typography>
          <Formik
            initialValues={{
              name: "",
              note: "",
            }}
            validationSchema={validUpdate}
            onSubmit={handleSubmitUpdate}
          >
            <Form className="add-form">
              <span style={{ margin: "10px 0", fontSize: "2rem" }}>
                Tên môn học cũ : {idUpdate.name}
              </span>
              <div className="add-subform">
                <label htmlFor="className">Tên môn học</label>
                <Field id="name" name="name" />
              </div>
              <span style={{ margin: "10px 0", fontSize: "2rem" }}>
                Ghi chú cũ : {idUpdate.note}
              </span>
              <div className="add-subform">
                <label htmlFor="className">Ghi chú</label>
                <Field id="note" name="note" />
              </div>
              <div className="add-action">
                <Button variant="contained" size="large" type="submit">
                  Cập nhật
                </Button>
                <Button
                  variant="contained"
                  size="large"
                  color="error"
                  onClick={handleCloseUpdate}
                >
                  Hủy
                </Button>
              </div>
            </Form>
          </Formik>
        </Box>
      </Modal>
    </>
  );
};

export default AdminSubject;
