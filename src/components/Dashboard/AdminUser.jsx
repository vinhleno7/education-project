import React, { useEffect, useState } from "react";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import Paper from "@mui/material/Paper";
import axios from "axios";
import { BASE_URL } from "../../constants";
import {
  Box,
  Button,
  Dialog,
  DialogActions,
  DialogTitle,
  IconButton,
  Modal,
  Typography,
} from "@mui/material";
import DeleteIcon from "@mui/icons-material/Delete";
import AddCircleOutlineIcon from "@mui/icons-material/AddCircleOutline";
import EditIcon from "@mui/icons-material/Edit";
import { useSelector, useDispatch } from "react-redux";
import { Field, Form, Formik } from "formik";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import CreateUser from "./CreateUser";
toast.configure();
const style = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 800,
  bgcolor: "background.paper",
  border: "2px solid #000",
  boxShadow: 24,
  p: 4,
};
const AdminUser = () => {
  const [data, setData] = useState([]);
  const { auth } = useSelector((state) => state);
  //create
  const [openModelCreate, setOpenModelCreate] = React.useState(false);
  const handleOpenCreate = () => setOpenModelCreate(true);
  const handleCloseCreate = () => setOpenModelCreate(false);
  //delete
  const [openDialogDelete, setOpenDialogDelete] = React.useState(false);
  const handleOpenDelete = () => setOpenDialogDelete(true);
  const handleCloseDelete = () => setOpenDialogDelete(false);
  const [idDel, setIdDel] = useState("");
  useEffect(() => {
    const config = {
      headers: {
        "Content-Type": "application/json",
        Authorization: auth.token,
      },
    };
    const fetchData = async () => {
      try {
        const res = await axios.get(`${BASE_URL}/api/getAllUser`, config);
        setData(res.data.users);
      } catch (error) {
        console.error(error);
      }
    };
    fetchData();
  }, []);
  const handleClickOpenDelete = (id) => {
    setIdDel(id);
    handleOpenDelete();
  };
  const handleClickDeleteId = (idDel) => {
    const config = {
      headers: {
        "Content-Type": "application/json",
        Authorization: auth.token,
      },
    };
    const fetchData = async () => {
      try {
        const res = await axios.delete(`${BASE_URL}/api/user/${idDel}`, config);
        toast.success(res.data.msg);
        handleCloseDelete();
        window.location.reload();
      } catch (error) {
        toast.error(error);
      }
    };
    fetchData();
  };
  return (
    <>
      <div style={{ marginBottom: "20px" }}>
        <Button
          variant="outlined"
          size="large"
          startIcon={<AddCircleOutlineIcon />}
          onClick={handleOpenCreate}
        >
          Create
        </Button>
      </div>
      <TableContainer component={Paper}>
        <Table sx={{ minWidth: 650 }} aria-label="simple table">
          <TableHead>
            <TableRow>
              <TableCell>Ảnh đại diện</TableCell>
              <TableCell align="left">Họ và Tên</TableCell>
              <TableCell align="left">Giới tính</TableCell>
              <TableCell align="left">Email</TableCell>
              <TableCell align="left">Số điện thoại</TableCell>
              <TableCell align="left">Chức vụ</TableCell>
              <TableCell align="left">MSSV || MSGV</TableCell>
              <TableCell align="left">Hành động</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {data.map((item, i) => (
              <TableRow
                key={i}
                sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
              >
                <TableCell component="th" scope="row">
                  <img
                    style={{ width: "40px", height: "40px" }}
                    src={item.avatar}
                    alt=""
                  />
                </TableCell>
                <TableCell align="left">{item.fullName}</TableCell>
                <TableCell align="left">{item.gender}</TableCell>
                <TableCell align="left">{item.email}</TableCell>
                <TableCell align="left">
                  {item.mobile ? item.mobile : "Chưa cập nhật"}
                </TableCell>
                <TableCell align="left">{item.role}</TableCell>
                <TableCell align="left">{item.studentID}</TableCell>
                <TableCell align="left">
                  <div>
                    <IconButton
                      aria-label="delete"
                      size="large"
                      onClick={() => handleClickOpenDelete(item._id)}
                    >
                      <DeleteIcon />
                    </IconButton>
                  </div>
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
      <Modal
        open={openModelCreate}
        onClose={handleCloseCreate}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={style}>
          <CreateUser handleCloseCreate={handleCloseCreate} />
        </Box>
      </Modal>
      <Dialog
        open={openDialogDelete}
        onClose={handleCloseDelete}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">
          {"Bạn có chắc chắn xóa môn học này"}
        </DialogTitle>
        <DialogActions>
          <DialogActions>
            <Button onClick={handleCloseDelete}>Hủy</Button>
            <Button
              variant="contained"
              color="error"
              onClick={() => handleClickDeleteId(idDel)}
              autoFocus
            >
              Xóa
            </Button>
          </DialogActions>
        </DialogActions>
      </Dialog>
    </>
  );
};

export default AdminUser;
