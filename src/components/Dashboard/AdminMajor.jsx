import React, { useEffect, useState } from "react";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import Paper from "@mui/material/Paper";
import axios from "axios";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { useSelector, useDispatch } from "react-redux";
import { BASE_URL } from "../../constants";
import {
  Box,
  Button,
  Dialog,
  DialogActions,
  DialogTitle,
  IconButton,
  Modal,
  Typography,
} from "@mui/material";
import DeleteIcon from "@mui/icons-material/Delete";
import AddCircleOutlineIcon from "@mui/icons-material/AddCircleOutline";
import EditIcon from "@mui/icons-material/Edit";
import { Field, Form, Formik } from "formik";
import * as Yup from "yup";
toast.configure();
const style = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 600,
  bgcolor: "background.paper",
  border: "2px solid #000",
  boxShadow: 24,
  p: 4,
};
const AdminMajor = () => {
  const [openModelCreate, setOpenModelCreate] = React.useState(false);
  const handleOpenCreate = () => setOpenModelCreate(true);
  const handleCloseCreate = () => setOpenModelCreate(false);

  const [openDialogDelete, setOpenDialogDelete] = React.useState(false);
  const handleOpenDelete = () => setOpenDialogDelete(true);
  const handleCloseDelete = () => setOpenDialogDelete(false);

  const [openModelUpdate, setOpenModelUpdate] = React.useState(false);
  const handleOpenUpdate = () => setOpenModelUpdate(true);
  const handleCloseUpdate = () => setOpenModelUpdate(false);

  const [idDel, setIdDel] = useState("");
  const [idUpdate, setIdUpdate] = useState("");

  const { auth } = useSelector((state) => state);
  const [data, setData] = useState([]);
  useEffect(() => {
    const config = {
      headers: {
        "Content-Type": "application/json",
        Authorization: auth.token,
      },
    };
    const fetchData = async () => {
      try {
        const res = await axios.get(`${BASE_URL}/api/major/getAll`, config);
        setData(res.data.majors);
      } catch (error) {
        console.error(error);
      }
    };
    fetchData();
  }, []);
  const handleClickOpenDelete = (id) => {
    setIdDel(id);
    handleOpenDelete();
  };
  const handleClickOpenUpdate = (id, item) => {
    setIdUpdate(id);
    setIdUpdate(item);
    handleOpenUpdate();
  };
  const valid = Yup.object({
    majorid: Yup.string().required("This is required!"),
    name: Yup.string().required("No name provided."),
  });
  const validUpdate = Yup.object({
    newmajorid: Yup.string().required("This is required!"),
    newnamemajor: Yup.string().required("No name provided."),
  });
  const handleSubmit = async (values) => {
    const majorid = values.majorid;
    const name = values.name;
    let data = {
      majorID: majorid,
      name: name,
    };
    const config = {
      headers: {
        "Content-Type": "application/json",
        Authorization: auth.token,
      },
    };
    axios
      .post(`${BASE_URL}/api/major/insert`, data, config)
      .then((result) => {
        toast.success(result.data.msg);
        handleCloseCreate();
        window.location.reload();
      })
      .catch((err) => {
        toast.error(err);
        console.log("err", err);
      });
  };
  const handleClickDeleteId = (idDel) => {
    console.log("iddel", idDel);
    const fetchData = async () => {
      try {
        const res = await axios.delete(
          `${BASE_URL}/api/major/deleteMajor/${idDel}`
        );
        toast.success(res);
        handleCloseDelete();
        window.location.reload();
      } catch (error) {
        toast.error(error);
      }
    };
    fetchData();
  };
  const handleSubmitUpdate = async (values) => {
    const newmajorid = values.newmajorid;
    const newnamemajor = values.newnamemajor;
    let data = {
      majorID: newmajorid,
      name: newnamemajor,
    };
    axios
      .put(`${BASE_URL}/api/major/updateMajor/${idUpdate}`, data)
      .then((result) => {
        toast.success(result.data);
        handleCloseUpdate();
        window.location.reload();
      })
      .catch((err) => {
        toast.error(err);
      });
  };
  console.log("idUpdate", idUpdate);

  return (
    <>
      <div style={{ marginBottom: "20px" }}>
        <Button
          variant="outlined"
          size="large"
          startIcon={<AddCircleOutlineIcon />}
          onClick={handleOpenCreate}
        >
          Create
        </Button>
      </div>
      <TableContainer component={Paper}>
        <Table sx={{ minWidth: 650 }} aria-label="simple table">
          <TableHead>
            <TableRow>
              <TableCell>Mã Khoa</TableCell>
              <TableCell align="left">Tên Khoa</TableCell>
              <TableCell align="left">Hành động</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {data.map((item, i) => (
              <TableRow
                key={i}
                sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
              >
                <TableCell component="th" scope="row">
                  {item.majorID}
                </TableCell>
                <TableCell align="left">{item.name}</TableCell>
                <TableCell align="left">
                  <div>
                    <IconButton
                      aria-label="delete"
                      size="large"
                      onClick={() => handleClickOpenDelete(item._id)}
                    >
                      <DeleteIcon />
                    </IconButton>
                    <IconButton
                      aria-label="edit"
                      size="large"
                      onClick={() => handleClickOpenUpdate(item._id, item)}
                    >
                      <EditIcon />
                    </IconButton>
                  </div>
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
      <Dialog
        open={openDialogDelete}
        onClose={handleCloseDelete}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">
          {"Bạn có chắc chắn xóa khoa này"}
        </DialogTitle>
        <DialogActions>
          <DialogActions>
            <Button onClick={handleCloseDelete}>Hủy</Button>
            <Button
              variant="contained"
              color="error"
              onClick={() => handleClickDeleteId(idDel)}
              autoFocus
            >
              Xóa
            </Button>
          </DialogActions>
        </DialogActions>
      </Dialog>
      <Modal
        open={openModelUpdate}
        onClose={handleCloseUpdate}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={style}>
          <Typography
            id="modal-modal-title"
            variant="h6"
            component="h2"
            sx={{
              textAlign: "center",
              fontSize: "2rem",
              color: "rgb(241, 110, 52)",
            }}
          >
            CẬP NHẬT KHOA
          </Typography>
          <Formik
            initialValues={{
              newmajorid: "",
              newnamemajor: "",
            }}
            validationSchema={validUpdate}
            onSubmit={handleSubmitUpdate}
          >
            <Form className="add-form">
              <span style={{ margin: "10px 0", fontSize: "2rem" }}>
                Mã khoa cũ : {idUpdate.majorID}
              </span>
              <div className="add-subform">
                <label htmlFor="className">Mã Khoa</label>
                <Field id="newmajorid" name="newmajorid" />
              </div>
              <span style={{ margin: "10px 0", fontSize: "2rem" }}>
                Tên khoa cũ : {idUpdate.name}
              </span>
              <div className="add-subform">
                <label htmlFor="className">Tên Khoa</label>
                <Field id="newnamemajor" name="newnamemajor" />
              </div>

              <div className="add-action">
                <Button variant="contained" size="large" type="submit">
                  Cập nhật
                </Button>
                <Button
                  variant="contained"
                  size="large"
                  color="error"
                  onClick={handleCloseUpdate}
                >
                  Hủy
                </Button>
              </div>
            </Form>
          </Formik>
        </Box>
      </Modal>
      <Modal
        open={openModelCreate}
        onClose={handleCloseCreate}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={style}>
          <Typography id="modal-modal-title" variant="h6" component="h2">
            Thêm khoa
          </Typography>
          <Formik
            initialValues={{
              majorid: "",
              namemajor: "",
            }}
            validationSchema={valid}
            onSubmit={handleSubmit}
          >
            <Form className="add-form">
              <div className="add-subform">
                <label htmlFor="className">Mã Khoa</label>
                <Field id="majorid" name="majorid" placeholder="CNTT" />
              </div>
              <div className="add-subform">
                <label htmlFor="className">Tên Khoa</label>
                <Field
                  id="name"
                  name="name"
                  placeholder="Công nghệ thông tin"
                />
              </div>

              <div className="add-action">
                <Button variant="contained" size="large" type="submit">
                  Thêm mới
                </Button>
                <Button
                  variant="contained"
                  size="large"
                  color="error"
                  onClick={handleCloseCreate}
                >
                  Hủy
                </Button>
              </div>
            </Form>
          </Formik>
        </Box>
      </Modal>
    </>
  );
};
export default AdminMajor;
