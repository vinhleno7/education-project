import { Typography } from "@mui/material";
import Button from "@mui/material/Button";
import axios from "axios";
import { Field, Form, Formik } from "formik";
import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import { toast } from "react-toastify";
import * as Yup from "yup";
import { BASE_URL } from "../../constants";
import "react-toastify/dist/ReactToastify.css";
toast.configure();
const CreateUser = ({ handleCloseCreate }) => {
  const { auth } = useSelector((state) => state);
  let navigate = useNavigate();
  const valid = Yup.object({
    fullname: Yup.string().required("No name provided."),
    codeid: Yup.string().required("No name provided."),
    email: Yup.string()
      .required("No email provided.")
      .email("Invalid email format"),
    password: Yup.string()
      .required("No password provided.")
      .min(6, "Password is too short - should be 8 chars minimum.")
      .matches(/[a-zA-Z]/, "Password can only contain Latin letters."),
    password_confirm: Yup.string()
      .required("Please confirm the password.")
      .oneOf([Yup.ref("password"), ""], "Passwords must match"),
  });
  const handleSubmit = async (values) => {
    const fullname = values.fullname;
    const codeid = values.codeid;
    const email = values.email;
    const password = values.password;
    let dataCreate = {
      fullName: fullname,
      studentID: codeid,
      email: email,
      password: password,
    };
    console.log("dataCreate", dataCreate);
    const config = {
      headers: {
        "Content-Type": "application/json",
        Authorization: auth.token,
      },
    };
    axios
      .post(`${BASE_URL}/api/register`, dataCreate, config)
      .then((result) => {
        console.log("result", result);
        toast.success("Thêm thành công");
        handleCloseCreate();
        window.location.reload();
      })
      .catch((err) => {
        toast.error(err);
        console.log("err", err);
      });
  };
  return (
    <>
      <Typography id="modal-modal-title" variant="h6" component="h2">
        Thêm người dùng
      </Typography>
      <Formik
        initialValues={{
          fullname: "",
          codeid: "",
          email: "",
          password: "",
          password_confirm: "",
        }}
        validationSchema={valid}
        onSubmit={handleSubmit}
      >
        {({ errors, touched }) => (
          <Form className="add-form">
            <div className="add-subform">
              <label htmlFor="className">Họ và Tên</label>
              <Field
                id="fullname"
                name="fullname"
                placeholder="Nhập tên đầy đủ của bạn..."
              />
            </div>
            {errors.fullname && touched.fullname ? (
              <div style={{ color: "red", textAlign: "left" }}>
                {errors.fullname}
              </div>
            ) : null}
            <div className="add-subform">
              <label htmlFor="className">Nhập Mã</label>
              <Field
                id="codeid"
                name="codeid"
                placeholder="Nhập mã sinh viên hoặc giảng viên..."
              />
            </div>
            {errors.codeid && touched.codeid ? (
              <div style={{ color: "red", textAlign: "left" }}>
                {errors.codeid}
              </div>
            ) : null}

            <div className="add-subform">
              <label htmlFor="className">Email</label>
              <Field
                id="email"
                name="email"
                placeholder="Nhập Email của bạn..."
              />
            </div>
            {errors.email && touched.email ? (
              <div style={{ color: "red", textAlign: "left" }}>
                {errors.email}
              </div>
            ) : null}

            <div className="add-subform">
              <label htmlFor="className">Mật khẩu</label>
              <Field
                type="password"
                id="password"
                name="password"
                placeholder="Nhập mật khẩu của bạn..."
              />
            </div>
            {errors.password && touched.password ? (
              <div style={{ color: "red", textAlign: "left" }}>
                {errors.password}
              </div>
            ) : null}
            <div className="add-subform">
              <label htmlFor="className">Nhập lại mật khẩu</label>
              <Field
                type="password"
                id="password_confirm"
                name="password_confirm"
                placeholder="Nhập lại mật khẩu của bạn..."
              />
            </div>
            {errors.password_confirm && touched.password_confirm ? (
              <div style={{ color: "red", textAlign: "left" }}>
                {errors.password_confirm}
              </div>
            ) : null}
            <Button
              sx={{
                display: "inline-block",
                width: "30%",
                margin: "22px 0",
                borderRadius: "30px",
              }}
              variant="contained"
              size="large"
              type="submit"
            >
              Đăng ký
            </Button>
            <Button
              sx={{
                display: "inline-block",
                width: "30%",
                margin: "22px 0",
                borderRadius: "30px",
              }}
              variant="contained"
              size="large"
              color="error"
              onClick={handleCloseCreate}
            >
              Hủy
            </Button>
          </Form>
        )}
      </Formik>
    </>
  );
};

export default CreateUser;
