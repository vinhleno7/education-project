import React from "react";

const Toast = ({ msg, handleShow, bgColor }) => {
  return (
    <div
      className={`toast show hidden position-fixed text-light ${bgColor}`}
      style={{
        top: "10px",
        right: "10px",
        minWidth: "250px",
        zIndex: 50,
        borderRadius: "5px",
        outline: "none",
        border: "1px solid #fff",
      }}
    >
      <div className={`toast-header text-light ${bgColor}`}>
        <strong className="mr-auto text-light" style={{ fontSize: "2rem" }}>
          {msg.title}
        </strong>
        <button
          className="ml-2 mb-1 close text-light"
          data-dismiss="toast"
          style={{ outline: "none" }}
          onClick={handleShow}
        >
          &times;
        </button>
      </div>
      <div className="toast-body" style={{ fontSize: "1.3rem" }}>
        {msg.body}
      </div>
    </div>
  );
};

export default Toast;
