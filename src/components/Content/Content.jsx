import React from "react";

// import ClassSubject from "../../pages/class-subject/ClassSubject";
import ClassSubject from "../../pages/class-list/ClassList";

function Content() {
  return (
    <div>
      <ClassSubject />
    </div>
  );
}

export default Content;
