import React, { useState, useRef } from "react";

import { Link } from "react-router-dom";
import { useNavigate } from "react-router-dom";
import { IoMenu } from "react-icons/io5";
import Logo from "../../assets/img/logo/logo.png";
import bellIcon from "../../assets/img/button/bell.svg";
import messageIcon from "../../assets/img/button/icons8-facebook-messenger.svg";
import Button from "@mui/material/Button";
import "./style.scss";
import Sidebar from "../Sidebar/Sidebar";
import classList from "../../Mock/classItem";
import { dataListSubject } from "../../Mock/data-classlist";

import studentLogo from "../../assets/img/user/student.png";
import teacherLogo from "../../assets/img/user/teacher.png";
import { useDispatch, useSelector } from "react-redux";
import { logout } from "../../redux/actions/authAction";
import { Menu, MenuItem } from "@mui/material";

function Header() {
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const { auth } = useSelector((state) => state);
  const { user } = auth;
  // console.log("user", user);

  const searchRef = useRef();
  const [showNav, setShowNav] = useState(false);
  const [handOut, setHandOut] = useState(false);
  const [resultSearch, setResultSearch] = useState([]);
  const studentID = !localStorage.getItem("Mssv")
    ? undefined
    : localStorage.getItem("Mssv");
  const email = !localStorage.getItem("Gmail")
    ? undefined
    : localStorage.getItem("Gmail");

  const role = !localStorage.getItem("Role")
    ? undefined
    : localStorage.getItem("Role");

  // const username = !localStorage.getItem("userName")
  //   ? undefined
  //   : localStorage.getItem("userName");
  // const email = !localStorage.getItem("userEmail")
  //   ? undefined
  //   : localStorage.getItem("userEmail");
  // const userimage = !localStorage.getItem("userImage")
  //   ? undefined
  //   : localStorage.getItem("userImage");
  let userimage;

  if (role == "student") {
    userimage = studentLogo;
  }
  if (role == "cbgd") {
    userimage = teacherLogo;
  }

  const handleLogout = () => {
    setHandOut(!handOut);
  };
  const handleSearch = () => {
    const key = searchRef.current.value;
    if (key !== "") {
      const result = dataListSubject.filter((item) =>
        item.name.toLowerCase().includes(key.toLowerCase())
      );
      let result3;
      dataListSubject.forEach((item) => {
        // console.log(item.classSubjects);
        result3 = item.classSubjects.filter((itemV) =>
          itemV.nameclass.toLowerCase().includes(key.toLowerCase())
        );
      });
      setResultSearch(result);
    } else {
      setResultSearch([]);
    }
  };
  const handleProfile = () => {
    navigate(`/profile/${user._id}`);
  };
  const [anchorEl, setAnchorEl] = React.useState(null);
  const open = Boolean(anchorEl);
  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };
  return (
    <>
      <div className="NavBar_wrapper">
        <div style={{ width: 50, height: 50, lineHeight: "65px" }}>
          <IoMenu
            style={{
              marginRight: 16,
              marginLeft: 8,
              fontSize: 25,
              cursor: "pointer"
            }}
            onClick={() => setShowNav(!showNav)}
          />
        </div>
        <div className="NavBar_logo">
          <Link to="/">
            <img src={Logo} alt="Logo" />
          </Link>
          {/* <h4 className="NavBar_logoHeading">Học Lập Trình Để Đi Làm</h4> */}
        </div>

        {/* <div className="NavBar_body d-flex-center">
          <div>
            <div className="Search_wrapper d-flex-center">
              <div className="Search_searchIcon"></div>
              <input
                className="Search_input"
                ref={searchRef}
                placeholder="Tìm kiếm khóa học, bài viết, video, ..."
                onChange={handleSearch}
              ></input>

              {resultSearch.length != 0 && (
                <div className="searchResult">
                  {resultSearch.map((item, index) => {
                    return <p key={index}>{item.name}</p>;
                  })}
                </div>
              )}
            </div>
          </div>
        </div> */}

        <div className="NavBar_actions">
          {auth.token && (
            <div id="navbar-actions-portal">
              {/* <Link to="/messenger">
                <img className="messageIcon" src={messageIcon} alt="Mess" />
              </Link>
              <img className="notificationIcon" src={bellIcon} alt="Bell" /> */}
              <img
                onClick={handleClick}
                aria-controls={open ? "basic-menu" : undefined}
                aria-haspopup="true"
                aria-expanded={open ? "true" : undefined}
                className="avatarIconHeader"
                src={user.avatar}
                alt="User"
              />
              {/* {handOut && (
                <div className="moreOption">
                  <Button variant="outlined" fullWidth onClick={handleProfile}>
                    Quản lý tài khoản
                  </Button>
                  <div className="line"></div>
                  <Button variant="outlined" fullWidth onClick={handleProfile}>
                    Tất cả lớp học
                  </Button>
                  <div className="line"></div>
                  <Button variant="outlined" fullWidth onClick={handleProfile}>
                    Trò chuyện
                  </Button>
                  <div className="line"></div>
                  <Button
                    className="btnOut"
                    variant="contained"
                    fullWidth
                    onClick={() => dispatch(logout())}
                  >
                    Đăng xuất
                  </Button>
                </div>
              )} */}
            </div>
          )}
          {!auth.token && (
            <Link to="/login" className="NavBar_loginBtn">
              Đăng nhập
            </Link>
          )}
          {
            // console.log(
            //   "🚀 ~ file: Header.jsx ~ line 41 ~ Header ~ showNav",
            //   showNav
            // )

            <Sidebar show={showNav} />
          }
        </div>
      </div>
      <Menu
        anchorEl={anchorEl}
        id="account-menu"
        open={open}
        onClose={handleClose}
        onClick={handleClose}
        PaperProps={{
          elevation: 0,
          sx: {
            overflow: "visible",
            filter: "drop-shadow(0px 2px 8px rgba(0,0,0,0.32))",
            mt: 1.5,
            "& .MuiAvatar-root": {
              width: 32,
              height: 32,
              ml: -0.5,
              mr: 1
            },
            "&:before": {
              content: '""',
              display: "block",
              position: "absolute",
              top: 0,
              right: 14,
              width: 10,
              height: 10,
              bgcolor: "background.paper",
              transform: "translateY(-50%) rotate(45deg)",
              zIndex: 0
            }
          }
        }}
        transformOrigin={{ horizontal: "right", vertical: "top" }}
        anchorOrigin={{ horizontal: "right", vertical: "bottom" }}
      >
        <MenuItem>
          <h2>Chào : {auth.user.fullName}</h2>
        </MenuItem>
        <MenuItem onClick={handleProfile}>
          <h2>Quản lý tài khoản</h2>
        </MenuItem>
        <MenuItem onClick={handleClose}>
          <h2>Danh sách lớp học</h2>
        </MenuItem>
        <MenuItem onClick={handleClose}>
          <h2>Cài đặt</h2>
        </MenuItem>
        {auth.user.role === "admin" ? (
          <MenuItem onClick={() => navigate("/dashboard")}>
            <h2>Quản lý Admin</h2>
          </MenuItem>
        ) : null}
        <MenuItem onClick={() => dispatch(logout())}>
          <Button className="btnOut" variant="contained" fullWidth>
            Đăng xuất
          </Button>
        </MenuItem>
      </Menu>
    </>
  );
}

export default Header;
