import React from "react";
import { IoAdd } from "react-icons/io5";
import { IoHome } from "react-icons/io5";
import { ImRoad } from "react-icons/im";
import { FaLightbulb } from "react-icons/fa";
import { GiBookCover } from "react-icons/gi";
import { useSelector, useDispatch } from "react-redux";
import { useNavigate } from "react-router-dom";
import "./style.scss";
import classList from "../../Mock/classItem";
import { dataListSubject } from "../../Mock/data-classlist";

import { NavLink } from "react-router-dom";
function Sidebar({ show }) {
  const { classSubject } = useSelector((state) => state);
  const navigate = useNavigate();
  // !theem khoa hoc theo classroom
  return (
    <div className={show ? "App_withSidebar focus" : "App_withSidebar"}>
      {/* <div className="App_sidebarWrap"> */}
      <div className="Sidebar_wrapper">
        <div>
          <NavLink to="/new-class">
            <div className="CreateButton_wrapper">
              <IoAdd className="iconAdd" />
            </div>
          </NavLink>
        </div>
        <ul className="Sidebar_list">
          <li>
            <NavLink className="Sidebar_a" activeclassname="active" to="/">
              <IoHome className="Sidebar_icon" />
              <span>Trang chủ</span>
            </NavLink>
          </li>

          <li>
            <NavLink
              className="Sidebar_a"
              activeclassname="active"
              to="/contact"
            >
              <ImRoad className="Sidebar_icon" />
              <span>Lộ trình</span>
            </NavLink>
          </li>
          <hr />
          <h1 style={{ color: "#f16e34" }}>Lớp học của tôi</h1>
          <div className="CSList">
            {classSubject.result > 0 ? (
              classSubject.classSubjects.map((item, index) => {
                return (
                  <li key={index}>
                    <NavLink
                      className="Sidebar_a"
                      activeclassname="active"
                      to={`/detail/${item._id}`}
                    >
                      <GiBookCover className="Sidebar_icon" />
                      <span key={index}>{item.name}</span>
                    </NavLink>
                  </li>
                );
              })
            ) : (
              <p>Bạn chưa tham gia lớp môn học nào cả</p>
            )}
          </div>

          <hr />
          <li>
            <NavLink
              className="Sidebar_a"
              activeclassname="active"
              to="/setting"
            >
              <FaLightbulb className="Sidebar_icon" />
              <span>Setting</span>
            </NavLink>
          </li>
        </ul>
      </div>
      {/* </div> */}
      {/* <div className="App_withSidebarContent"></div> */}
    </div>
  );
}

export default Sidebar;
