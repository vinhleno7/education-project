import { GLOBALTYPES } from "./globalTypes";
import {
  postDataAPI,
  getDataAPI,
  patchDataAPI,
  deleteDataAPI
} from "../../utils/fetchData";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

toast.configure();
export const POST_TYPES = {
  CREATE_POST: "CREATE_POST",
  LOADING_POST: "LOADING_POST",
  GET_POSTS: "GET_POSTS",
  UPDATE_POST: "UPDATE_POST",
  DELETE_POST: "DELETE_POST"
};

export const createPost =
  ({ content, auth, myCS, myGroup, userID }) =>
  async (dispatch) => {
    console.log({ content, auth, myCS, myGroup });
    const classSubjectID = myCS ? myCS._id : "";
    const groupID = myGroup ? myGroup._id : "";
    try {
      const res = await postDataAPI(
        "post/insert",
        { content, classSubjectID, groupID, userID },
        auth.token
      );

      dispatch({
        type: POST_TYPES.CREATE_POST,
        payload: { ...res.data.newPost, user: auth.user }
      });
      toast.success(res.data.msg);
    } catch (err) {
      toast.error(err.response.data.msg);
    }
  };

export const getPosts =
  ({ auth, myCS }) =>
  async (dispatch) => {
    try {
      dispatch({ type: POST_TYPES.LOADING_POST, payload: true });
      const res = await getDataAPI(`post/getPosts/${myCS._id}`, auth.token);
      dispatch({
        type: POST_TYPES.GET_POSTS,
        payload: { ...res.data, page: 2 }
      });
      // toast.success(res.data.msg);
      dispatch({ type: POST_TYPES.LOADING_POST, payload: false });
    } catch (err) {
      // toast.error(err.response.data.msg);
    }
  };

export const updatePost =
  ({ content, auth, status }) =>
  async (dispatch) => {
    if (status.content === content) return;

    try {
      dispatch({ type: GLOBALTYPES.ALERT, payload: { loading: true } });

      const res = await patchDataAPI(
        `post/updatePost/${status._id}`,
        { content },
        auth.token
      );
      dispatch({
        type: POST_TYPES.UPDATE_POST,
        payload: { ...res.data.newPost, user: auth.user }
      });
      toast.success(res.data.msg);
    } catch (err) {
      toast.error(err.response.data.msg);
    }
  };

export const deletePost =
  ({ post, auth }) =>
  async (dispatch) => {
    dispatch({ type: POST_TYPES.DELETE_POST, payload: post });

    try {
      const res = await deleteDataAPI(`post/delete/${post._id}`, auth.token);
      toast.success(res.data.msg);
    } catch (err) {
      toast.error(err.response.data.msg);
    }
  };
