import {
  getDataAPI,
  postDataAPI,
  deleteDataAPI,
  putDataAPI,
} from "../../utils/fetchData";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { GROUP_TYPES } from "./groupAction";
toast.configure();
export const TYPES_CS = {
  CREATE_CS: "CREATE_CS",
  LOADING_CS: "LOADING_CS",
  GET_CS: "GET_CS",
  GET_DETAIL: "GET_DETAIL",
  UPDATE_CS: "UPDATE_CS",
  ADD_STUDENT: "ADD_STUDENT",
  DELETE_STUDENT: "DELETE_STUDENT",
  ADD_CS: "ADD_CS",
  DELETE_CS: "DELETE_CS",
};

export const getCSs = (token) => async (dispatch) => {
  try {
    dispatch({ type: TYPES_CS.LOADING_CS, payload: true });

    const res = await getDataAPI(`classSubject/getCSByIdUser`, token);

    dispatch({
      type: TYPES_CS.GET_CS,
      payload: { ...res.data },
    });

    dispatch({ type: TYPES_CS.LOADING_CS, payload: false });
  } catch (err) {
    dispatch({ type: TYPES_CS.LOADING_CS, payload: false });
    toast.error(err.response.data.msg);
  }
};

export const getDetailCS = (auth, id) => async (dispatch) => {
  try {
    const res = await getDataAPI(
      `classSubject/getASubjectClass/${id}`,
      auth.token
    );
    dispatch({ type: TYPES_CS.GET_DETAIL, payload: res.data });
    dispatch({ type: GROUP_TYPES.GET_GROUP, payload: res.data });
  } catch (err) {
    dispatch({ type: TYPES_CS.LOADING_CS, payload: false });
    toast.error(err.response.data.msg);
  }
};

export const addStudentCS =
  ({ auth, classSubjectID, studentID }) =>
  async (dispatch) => {
    try {
      const res = await postDataAPI(
        "classSubject/addStudent",
        { classSubjectID, studentID },
        auth.token
      );
      dispatch({
        type: TYPES_CS.ADD_STUDENT,
        payload: res.data,
      });

      toast.success(res.data.msg);
    } catch (err) {
      toast.error(err.response.data.msg);
    }
  };
export const deleteStudentCS =
  ({ auth, classSubjectID, studentID }) =>
  async (dispatch) => {
    const token = auth.token;
    try {
      const res = await postDataAPI(
        "classSubject/deleteStudent",
        { classSubjectID, studentID },
        token
      );

      toast.success(res.data.msg);
      console.log("res student", res);

      dispatch({
        type: TYPES_CS.DELETE_STUDENT,
        payload: res.data,
      });
      dispatch({
        type: GROUP_TYPES.UPDATE_GROUP,
        payload: res.data,
      });
    } catch (err) {
      toast.error(err.response.data.msg);
    }
  };
// create class_subject

export const createClassSubject =
  ({ dataAddCS, auth }) =>
  async (dispatch) => {
    try {
      dispatch({ type: "NOTIFY", payload: { loading: true } });
      const res = await postDataAPI(
        "classSubject/insert",
        dataAddCS,
        auth.token
      );
      dispatch({
        type: TYPES_CS.ADD_CS,
        payload: res.data,
      });
      dispatch({ type: "NOTIFY", payload: toast.success(res.data.msg) });
    } catch (error) {
      toast.error(error.response.data.msg);
    }
  };

// delete class_subject

export const deleteClassSubject =
  ({ auth, id }) =>
  async (dispatch) => {
    try {
      dispatch({ type: "NOTIFY", payload: { loading: true } });
      const res = await deleteDataAPI(`classSubject/delete/${id}`, auth.token);
      console.log("res", res);
      dispatch({
        type: TYPES_CS.DELETE_CS,
        payload: res.data,
      });
      dispatch({ type: "NOTIFY", payload: toast.success(res.data) });
      window.location.reload();
    } catch (err) {
      dispatch({ type: "NOTIFY", payload: { loading: false } });
      toast.error(err.response.data.msg);
    }
  };
//Update ClassSubject
export const updateClassSubject =
  ({ newName, id, auth }) =>
  async (dispatch) => {
    console.log("dataupdate", newName);
    try {
      const res = await putDataAPI(
        `classSubject/update/${id}`,
        { newName },
        auth.token
      );
      console.log("Res", res);

      dispatch({
        type: TYPES_CS.UPDATE_CS,
        payload: res.data,
      });
      toast.success(res.data.msg);
      window.location.reload();
    } catch (err) {
      toast.error(err.response.data.msg);
    }
  };
