import { imageUpload } from "../../utils/imageUpload";
import { GLOBALTYPES } from "./globalTypes";
import {
  getDataAPI,
  getDataAPIProfile,
  patchDataAPI,
  patchDataApiProfile,
  postDataAPI,
} from "../../utils/fetchData";
import { toast } from "react-toastify";
export const PROFILE_TYPES = {
  LOADING: "LOADING",
  GET_USER: "GET_USER",
  GET_ID: "GET_PROFILE_ID",
};
export const getProfileUsers =
  ({ id, auth, users }) =>
  async (dispatch) => {
    try {
      dispatch({ type: "NOTIFY", payload: { loading: true } });
      const res = await getDataAPI(`user/${id}`, auth.token);
      dispatch({
        type: PROFILE_TYPES.GET_USER,
        payload: res.data,
      });
      dispatch({ type: "NOTIFY", payload: { loading: false } });
    } catch (err) {
      dispatch({
        type: "NOTIFY",
        payload: { error: err.response.data.msg },
      });
      toast.success(err.response.data.msg);
    }
  };
export const updateProfileUser =
  ({ userData, avatar, auth }) =>
  async (dispatch) => {
    try {
      let media;
      dispatch({ type: "NOTIFY", payload: { loading: true } });

      if (avatar) media = await imageUpload([avatar]);

      const res = await patchDataAPI(
        "user",
        {
          ...userData,
          avatar: avatar ? media[0].url : auth.user.avatar,
        },
        auth.token
      );
      dispatch({
        type: GLOBALTYPES.AUTH,
        payload: {
          ...auth,
          user: {
            ...auth.user,
            ...userData,
            avatar: avatar ? media[0].url : auth.user.avatar,
          },
        },
      });
      dispatch({ type: "NOTIFY", payload: toast.success(res.data.msg) });
    } catch (error) {
      toast.error(error.response.data.msg);
    }
  };
