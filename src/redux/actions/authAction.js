import { postDataAPI } from "../../utils/fetchData";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

toast.configure();

export const TYPES = {
  AUTH: "AUTH",
};

export const login = (data) => async (dispatch) => {
  try {
    dispatch({ type: "NOTIFY", payload: { loading: true } });
    const res = await postDataAPI("login", data);

    dispatch({
      type: "AUTH",
      payload: { token: res.data.access_token, user: res.data.user },
    });
    localStorage.setItem("firstLogin", true);
    toast.success(res.data.msg);
  } catch (err) {
    toast.error(err.response.data.msg);
  }

  setTimeout(() => {
    dispatch({ type: "NOTIFY", payload: {} });
  }, 3000);
};

export const refreshToken = () => async (dispatch) => {
  const firstLogin = localStorage.getItem("firstLogin");
  if (firstLogin) {
    dispatch({ type: "NOTIFY", payload: { loading: true } });
    try {
      const res = await postDataAPI("refresh_token");
      dispatch({
        type: "AUTH",
        payload: { token: res.data.access_token, user: res.data.user },
      });
      dispatch({ type: "NOTIFY", payload: {} });
    } catch (err) {
      toast.error(err.response.data.msg);
    }
    setTimeout(() => {
      dispatch({ type: "NOTIFY", payload: {} });
    }, 3000);
  }
};

export const register = (data) => async (dispatch) => {
  try {
    dispatch({ type: "NOTIFY", payload: { loading: true } });
    const res = await postDataAPI("register", data);
    localStorage.setItem("firstLogin", true);
    toast.success(res.data.msg);

    window.location.href = "/";
  } catch (err) {
    toast.error(err.response.data.msg);
  }
  setTimeout(() => {
    dispatch({ type: "NOTIFY", payload: {} });
  }, 3000);
};
export const logout = () => async (dispatch) => {
  try {
    localStorage.removeItem("firstLogin");
    await postDataAPI("logout");
    window.location.href = "/";
    toast.success("Logout Successful !");
  } catch (err) {
    toast.error(err.response.data.msg);
  }
  setTimeout(() => {
    dispatch({ type: "NOTIFY", payload: {} });
  }, 3000);
};
