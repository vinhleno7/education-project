import { GLOBALTYPES } from "./globalTypes";
import {
  postDataAPI,
  getDataAPI,
  patchDataAPI,
  deleteDataAPI
} from "../../utils/fetchData";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

toast.configure();

export const REQUEST_GROUP_TYPES = {
  GET_ALL_REQUEST_GR: "GET_ALL_REQUEST_GR",
  CREATE_REQUEST_GR: "CREATE_REQUEST_GR",
  REPLY_REQUEST_GR: "REPLY_REQUEST_GR"
};

export const createRequestChangeGroup =
  ({ title, reasonChange, myGroup, groupChange, auth, myCS }) =>
  async (dispatch) => {
    try {
      const content = reasonChange;
      const groupID = myGroup._id;
      const classSubjectID = myCS._id;
      const groupToChangeID = groupChange._id;
      const studentID = auth.user._id;
      const teacherID = myCS.teacher._id;

      const res = await postDataAPI(
        "requestChangeGroup/create",
        {
          title,
          content,
          groupID,
          classSubjectID,
          groupToChangeID,
          studentID,
          teacherID
        },
        auth.token
      );
      dispatch({
        type: REQUEST_GROUP_TYPES.CREATE_REQUEST_GR,
        payload: res.data
      });
      toast.success(res.data.msg);
    } catch (err) {
      toast.error(err.response.data.msg);
    }
  };

export const getAllRequestGroup = (auth, id) => async (dispatch) => {
  try {
    const res = await getDataAPI(
      `requestChangeGroup/getAllByIdUser/${id}`,
      auth.token
    );

    toast.success(res.data.msg);
    dispatch({
      type: REQUEST_GROUP_TYPES.GET_ALL_REQUEST_GR,
      payload: res.data
    });
  } catch (err) {
    // toast.error(err.response.data.msg);
  }
};
export const replyToRequestChangeGroup =
  ({ myTempRGR, status, auth }) =>
  async (dispatch) => {
    const requestChangeGroupID = myTempRGR._id;
    const token = auth.token;
    try {
      const res = await postDataAPI(
        "requestChangeGroup/reply",
        { requestChangeGroupID, status },
        token
      );

      toast.success(res.data.msg);
      dispatch({
        type: REQUEST_GROUP_TYPES.REPLY_REQUEST_GR,
        payload: res.data
      });
    } catch (err) {
      toast.error(err.response.data.msg);
    }
  };
