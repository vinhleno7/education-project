import { GLOBALTYPES } from "./globalTypes";
import {
  postDataAPI,
  getDataAPI,
  patchDataAPI,
  deleteDataAPI
} from "../../utils/fetchData";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

toast.configure();

export const GROUP_TYPES = {
  LOADING_GROUP: "LOADING_GROUP",
  GET_GROUP: "GET_GROUP",
  CREATE_GROUP: "CREATE_GROUP",
  ADD_STUDENT: "ADD_STUDENT",
  DELETE_STUDENT: "DELETE_GROUP",
  UPDATE_GROUP: "UPDATE_GROUP",
  DELETE_GROUP: "DELETE_GROUP"
};

export const insertGroup =
  ({ auth, values, myCS }) =>
  async (dispatch) => {
    const token = auth.token;
    const name = values.namegroup;
    const classSubjectID = myCS._id;

    try {
      const res = await postDataAPI(
        `group/insert`,
        { name, classSubjectID },
        token
      );

      dispatch({
        type: GROUP_TYPES.CREATE_GROUP,
        payload: res.data
      });

      toast.success(res.data.msg);
    } catch (err) {
      toast.error(err.response.data.msg);
    }
  };

export const addMembers =
  ({ auth, values, myStudents }) =>
  async (dispatch) => {
    const token = auth.token;
    const groupName = values.namegroup;
    let studentID = [];
    for (const student of myStudents) {
      studentID.push(student._id);
    }

    try {
      const res = await postDataAPI(
        `group/addMember`,
        { groupName, studentID },
        token
      );

      dispatch({
        type: GROUP_TYPES.UPDATE_GROUP,
        payload: res.data
      });
      toast.success(res.data.msg);
    } catch (err) {
      toast.error(err.response.data.msg);
    }
  };

export const deleteMember =
  ({ auth, name, id }) =>
  async (dispatch) => {
    const token = auth.token;
    const groupName = name;
    const studentID = id;

    try {
      const res = await postDataAPI(
        `group/deleteMember`,
        { groupName, studentID },
        token
      );

      dispatch({
        type: GROUP_TYPES.UPDATE_GROUP,
        payload: res.data
      });
      toast.success(res.data.msg);
    } catch (err) {
      toast.error(err.response.data.msg);
    }
  };

export const updateGroup =
  ({ auth, newName, item }) =>
  async (dispatch) => {
    console.log("🚀 ~ file: groupAction.js ~ line 101 ~ item", item);
    const token = auth.token;
    const groupID = item._id;

    try {
      const res = await patchDataAPI(
        `group/updateGroup`,
        { newName, groupID },
        token
      );

      dispatch({
        type: GROUP_TYPES.UPDATE_GROUP,
        payload: res.data
      });
      toast.success(res.data.msg);
    } catch (err) {
      toast.error(err.response.data.msg);
    }
  };

export const deleteGroup =
  ({ auth, item }) =>
  async (dispatch) => {
    const group = item;
    try {
      const res = await deleteDataAPI(
        `group/deleteGroup/${group._id}`,
        auth.token
      );

      dispatch({
        type: GROUP_TYPES.DELETE_GROUP,
        payload: group
      });
      toast.success(res.data.msg);
    } catch (err) {
      toast.error(err.response.data.msg);
    }
  };
