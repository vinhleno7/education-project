import { GLOBALTYPES, EditData, DeleteData } from "./globalTypes";
import { POST_TYPES } from "./postAction";
import {
  postDataAPI,
  patchDataAPI,
  deleteDataAPI
} from "../../utils/fetchData";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
toast.configure();

export const createComment = (post, newComment, auth) => async (dispatch) => {
  const newPost = { ...post, comments: [...post.comments, newComment] };

  try {
    const data = { ...newComment, postId: post._id };
    const res = await postDataAPI("comment/createComment", data, auth.token);

    const newData = { ...res.data.newComment, author: auth.user };
    const newPost = { ...post, comments: [...post.comments, newData] };
    dispatch({ type: POST_TYPES.UPDATE_POST, payload: newPost });
    toast.success(res.data.msg);
  } catch (err) {
    toast.error(err.response.data.msg);
  }
};

// export const updateComment =
//   ({ comment, post, content, auth }) =>
//   async (dispatch) => {
//     const newComments = EditData(post.comments, comment._id, {
//       ...comment,
//       content
//     });
//     const newPost = { ...post, comments: newComments };

//     dispatch({ type: POST_TYPES.UPDATE_POST, payload: newPost });
//     try {
//       patchDataAPI(`comment/${comment._id}`, { content }, auth.token);
//     } catch (err) {
//       dispatch({
//         type: GLOBALTYPES.ALERT,
//         payload: { error: err.response.data.msg }
//       });
//     }
//   };

export const deleteComment =
  ({ post, comment, auth }) =>
  async (dispatch) => {
    console.log({ post, comment, auth });
    const deleteArr = [
      ...post.comments.filter((cm) => cm.reply === comment._id),
      comment
    ];

    const newPost = {
      ...post,
      comments: post.comments.filter(
        (cm) => !deleteArr.find((da) => cm._id === da._id)
      )
    };

    dispatch({ type: POST_TYPES.UPDATE_POST, payload: newPost });

    try {
      const res = await deleteDataAPI(
        `comment/delete/${comment._id}`,
        auth.token
      );
      toast.success(res.data.msg);
    } catch (err) {
      toast.error(err.response.data.msg);
    }
  };
