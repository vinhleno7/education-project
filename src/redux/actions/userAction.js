import { GLOBALTYPES } from "./globalTypes";
import {
  postDataAPI,
  getDataAPI,
  patchDataAPI,
  deleteDataAPI
} from "../../utils/fetchData";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

toast.configure();
export const USER_TYPES = {
  GET_ALL: "GET_ALL"
};

export const getAllUser = (auth) => async (dispatch) => {
  try {
    const res = await getDataAPI("getAllUser", auth.token);
    dispatch({ type: USER_TYPES.GET_ALL, payload: res.data });
  } catch (err) {
    toast.error(err.response.data.msg);
  }
};
