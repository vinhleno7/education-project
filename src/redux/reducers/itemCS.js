import { TYPES_CS } from "../actions/classSubjectAction";
// import { EditData } from "../actions/globalTypes";
const initialState = {};

const itemCSReducer = (state = initialState, action) => {
  switch (action.type) {
    case TYPES_CS.GET_DETAIL:
      return action.payload;
    case TYPES_CS.ADD_STUDENT:
      return action.payload.myCS;
    case TYPES_CS.DELETE_STUDENT:
      return action.payload.myCS;
    default:
      return state;
  }
};

export default itemCSReducer;
