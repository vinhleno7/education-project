import { GROUP_TYPES } from "../actions/groupAction";
import { EditData, DeleteData } from "../actions/globalTypes";
const initialState = {
  loading: false,
  groups: [],
  result: 0
};

const groupReducer = (state = initialState, action) => {
  switch (action.type) {
    case GROUP_TYPES.GET_GROUP:
      return {
        ...state,
        groups: action.payload.groups,
        result: action.payload.groups.length
      };
    case GROUP_TYPES.CREATE_GROUP:
      return {
        ...state,
        groups: [action.payload.group, ...state.groups]
      };
    case GROUP_TYPES.DELETE_GROUP:
      return {
        ...state,
        groups: DeleteData(state.groups, action.payload._id)
      };
    case GROUP_TYPES.UPDATE_GROUP:
      return {
        ...state,
        groups: EditData(
          state.groups,
          action.payload.group._id,
          action.payload.group
        )
      };
    default:
      return state;
  }
};

export default groupReducer;
