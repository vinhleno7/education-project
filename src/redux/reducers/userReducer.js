import { USER_TYPES } from "../actions/userAction";

const initialState = [];

const userReducer = (state = initialState, action) => {
  switch (action.type) {
    case USER_TYPES.GET_ALL:
      return action.payload.users;
    default:
      return state;
  }
};

export default userReducer;
