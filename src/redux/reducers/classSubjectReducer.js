import { TYPES_CS } from "../actions/classSubjectAction";
import { DeleteData } from "../actions/globalTypes";
// import { EditData } from "../actions/globalTypes";
const initialState = {
  loading: false,
  classSubjects: [],
  result: 0,
  // myCS: {}
};

const classSubjectReducer = (state = initialState, action) => {
  switch (action.type) {
    case TYPES_CS.LOADING_CS:
      return {
        ...state,
        loading: action.payload,
      };
    case TYPES_CS.GET_CS:
      return {
        ...state,
        classSubjects: action.payload.myCSs,
        result: action.payload.result,
      };
    case TYPES_CS.ADD_STUDENT:
      return {
        ...state,
        classSubjects: [action.payload.myCS, ...state.classSubjects],
      };
    case TYPES_CS.DELETE_STUDENT:
      return {
        ...state,
        classSubjects: [action.payload.myCS, ...state.classSubjects],
      };
    case TYPES_CS.ADD_CS:
      return {
        ...state,
        classSubjects: [action.payload.myCS, ...state.classSubjects],
      };
    case TYPES_CS.DELETE_CS:
      return {
        ...state,
        classSubjects: DeleteData(state.classSubjects, action.payload.myCS),
      };
    default:
      return state;
  }
};

export default classSubjectReducer;
