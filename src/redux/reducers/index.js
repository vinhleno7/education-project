import { combineReducers } from "redux";
import auth from "./authReducer";
import notify from "./notifyReducer";
import classSubject from "./classSubjectReducer";
import itemCS from "./itemCS";
import post from "./postReducer";
import status from "./statusReducer";
import group from "./groupReducer";
import user from "./userReducer";
import profile from "./profileReducer";
import requestGr from "./requestChangeGroupReducer";
export default combineReducers({
  auth,
  notify,
  classSubject,
  itemCS,
  post,
  status,
  group,
  user,
  profile,
  requestGr
});
