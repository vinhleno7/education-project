import { REQUEST_GROUP_TYPES } from "../actions/requestChangeGroup";
// import { EditData } from "../actions/globalTypes";
const initialState = {
  requestGrs: [],
  result: 0,
  status: ""
};

const requestChangeGroupReducer = (state = initialState, action) => {
  switch (action.type) {
    case REQUEST_GROUP_TYPES.GET_ALL_REQUEST_GR:
      return {
        ...state,
        requestGrs: action.payload,
        result: action.payload.length
      };
    case REQUEST_GROUP_TYPES.CREATE_REQUEST_GR:
      return {
        ...state,
        requestGrs: [action.payload.requestChangeGroup, ...state.requestGrs],
        result: state.result + 1
      };
    case REQUEST_GROUP_TYPES.REPLY_REQUEST_GR:
      return {
        ...state,
        status: action.payload.status
      };
    default:
      return state;
  }
};

export default requestChangeGroupReducer;
