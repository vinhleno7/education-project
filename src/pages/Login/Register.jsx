import { IconButton } from "@mui/material";
import Button from "@mui/material/Button";
import { Field, Form, Formik } from "formik";
import React from "react";
import { FaUserCheck } from "react-icons/fa";
import { HiOutlineMail } from "react-icons/hi";
import { RiLockPasswordLine } from "react-icons/ri";
import { Link, useNavigate } from "react-router-dom";
import Logo from "../../assets/img/logo/logo.png";
import * as Yup from "yup";
import { BASE_URL } from "../../constants";
import "./style.scss";
import { toast } from "react-toastify";
import { useDispatch } from "react-redux";
import { register } from "../../redux/actions/authAction";
const Register = ({ handleClick }) => {
  let navigate = useNavigate();
  const valid = Yup.object({
    fullname: Yup.string().required("No name provided."),
    codeid: Yup.string().required("No name provided."),
    email: Yup.string()
      .required("No email provided.")
      .email("Invalid email format"),
    password: Yup.string()
      .required("No password provided.")
      .min(6, "Password is too short - should be 8 chars minimum.")
      .matches(/[a-zA-Z]/, "Password can only contain Latin letters."),
    password_confirm: Yup.string()
      .required("Please confirm the password.")
      .oneOf([Yup.ref("password"), ""], "Passwords must match")
  });
  const dispatch = useDispatch();
  const handleSubmit = async (values) => {
    const fullname = values.fullname;
    const codeid = values.codeid;
    const email = values.email;
    const password = values.password;
    let dataRegister = {
      fullName: fullname,
      studentID: codeid,
      email: email,
      password: password
    };
    dispatch(register(dataRegister));

    // const check = localStorage.getItem("firstLogin");

    // console.log(` ${check}`);
    // console.log(typeof check);
    // if (localStorage.getItem("firstLogin") == "true") {
    //   console.log("GO LOGIN");
    //   handleClick();
    // }
  };
  return (
    <div className="Login_container">
      <div className="Login_content">
        <div className="Login_header">
          <Link to="/">
            <img className="Login_logo" src={Logo} alt="F8" />
          </Link>
          <h1 className="Login_title">Đăng ký </h1>
        </div>

        <div className="Login_body">
          <div className="Login_mainStep">
            <Formik
              // initialValues={{
              //   Email: "",
              //   Password: "",
              // }}
              // onSubmit={async (values) => {
              //   await new Promise((r) => setTimeout(r, 500));
              //   // alert(JSON.stringify(values, null, 2));
              //   const email = values.Email;
              //   const mssv = email.substring(0, email.lastIndexOf("@"));
              //   const domain = email.substring(email.lastIndexOf("@") + 1);
              //   const role = domain.substring(0, domain.indexOf("."));

              //   localStorage.clear();

              //   localStorage.setItem("Gmail", email);
              //   localStorage.setItem("Mssv", mssv);
              //   localStorage.setItem("Role", role);
              //   navigate("/");
              // }}
              initialValues={{
                fullname: "",
                codeid: "",
                email: "",
                password: "",
                password_confirm: ""
              }}
              validationSchema={valid}
              onSubmit={handleSubmit}
            >
              {({ errors, touched }) => (
                <Form className="add-form">
                  <div className="SigninButton_wrapper">
                    <IconButton className="SigninButton_icon">
                      <FaUserCheck />
                    </IconButton>
                    <Field
                      id="fullname"
                      name="fullname"
                      placeholder="Nhập tên đầy đủ của bạn..."
                    />
                    {errors.fullname && touched.fullname ? (
                      <div style={{ color: "red", textAlign: "left" }}>
                        {errors.fullname}
                      </div>
                    ) : null}
                  </div>
                  <div className="SigninButton_wrapper">
                    <IconButton className="SigninButton_icon">
                      <FaUserCheck />
                    </IconButton>
                    <Field
                      id="codeid"
                      name="codeid"
                      placeholder="Nhập mã sinh viên hoặc giảng viên..."
                    />
                    {errors.codeid && touched.codeid ? (
                      <div style={{ color: "red", textAlign: "left" }}>
                        {errors.codeid}
                      </div>
                    ) : null}
                  </div>
                  <div className="SigninButton_wrapper">
                    <IconButton className="SigninButton_icon">
                      <HiOutlineMail />
                    </IconButton>
                    <Field
                      id="email"
                      name="email"
                      placeholder="Nhập Email của bạn..."
                    />
                    {errors.email && touched.email ? (
                      <div style={{ color: "red", textAlign: "left" }}>
                        {errors.email}
                      </div>
                    ) : null}
                  </div>
                  <div className="SigninButton_wrapper">
                    <IconButton className="SigninButton_icon">
                      <RiLockPasswordLine />
                    </IconButton>
                    <Field
                      type="password"
                      id="password"
                      name="password"
                      placeholder="Nhập mật khẩu của bạn..."
                    />
                    {errors.password && touched.password ? (
                      <div style={{ color: "red", textAlign: "left" }}>
                        {errors.password}
                      </div>
                    ) : null}
                  </div>
                  <div className="SigninButton_wrapper">
                    <IconButton className="SigninButton_icon">
                      <RiLockPasswordLine />
                    </IconButton>
                    <Field
                      type="password"
                      id="password_confirm"
                      name="password_confirm"
                      placeholder="Nhập lại mật khẩu của bạn..."
                    />
                    {errors.password_confirm && touched.password_confirm ? (
                      <div style={{ color: "red", textAlign: "left" }}>
                        {errors.password_confirm}
                      </div>
                    ) : null}
                  </div>
                  <Button
                    sx={{
                      display: "inline-block",
                      width: "50%",
                      margin: "22px 0",
                      borderRadius: "30px"
                    }}
                    variant="contained"
                    size="large"
                    type="submit"
                  >
                    Đăng ký
                  </Button>
                </Form>
              )}
            </Formik>
          </div>
          <p className="Login_dontHaveAcc">
            Bạn đã có tài khoản?
            <Button onClick={handleClick}> Đăng nhập</Button>
          </p>
        </div>
      </div>

      <div className="Login_about">
        <Link to="/about-us" target="_blank" rel="noopener noreferrer">
          Giới thiệu về LV Edu
        </Link>
        <span>|</span>
        <Link
          to="https://url.mycv.vn/?ref=f8_register&amp;to=f8_youtube"
          target="_blank"
          rel="noopener noreferrer"
        >
          LV Edu trên Youtube
        </Link>
        <span>|</span>
        <Link
          to="https://url.mycv.vn/?ref=f8_register&amp;to=f8_fb_group"
          target="_blank"
          rel="noopener noreferrer"
        >
          LV Edu trên Facebook
        </Link>
      </div>
    </div>
  );
};

export default Register;
