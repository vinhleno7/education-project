import React, { useEffect } from "react";
import { GoogleLogin } from "react-google-login";
import { useNavigate } from "react-router-dom";
import "./style.scss";
import { Link } from "react-router-dom";
import Logo from "../../assets/img/logo/logo.png";
import gmailIcon from "../../assets/img/button/gmail-18px.svg";
import googleIcon from "../../assets/img/button/google-18px.svg";
import facebookIcon from "../../assets/img/button/facebook-18px.svg";
import githubIcon from "../../assets/img/button/github-18px.svg";
import { Formik, Field, Form } from "formik";
import { IconButton, Typography } from "@mui/material";
import Button from "@mui/material/Button";
import { HiOutlineMail } from "react-icons/hi";
import { RiLockPasswordLine } from "react-icons/ri";
import * as Yup from "yup";
import { login, refreshToken } from "../../redux/actions/authAction";
import { useDispatch, useSelector } from "react-redux";
const Login = ({ handleClick }) => {
  const navigate = useNavigate();
  const valid = Yup.object({
    email: Yup.string()
      .required("No email provided.")
      .email("Invalid email format"),
    password: Yup.string()
      .required("No password provided.")
      .min(6, "Password is too short - should be 8 chars minimum.")
      .matches(/[a-zA-Z]/, "Password can only contain Latin letters."),
  });
  const dispatch = useDispatch();
  const { auth } = useSelector((state) => state);
  useEffect(() => {
    if (auth.token) navigate("/");
  }, [auth.token, navigate]);
  const handleSubmit = async (value) => {
    const data = {
      email: value.email,
      password: value.password,
    };
    dispatch(login(data));
    navigate("/");
  };
  //! Vẫn chưa logout
  return (
    <div className="Login_container">
      <div className="Login_content">
        <div className="Login_header">
          <Link to="/">
            <img className="Login_logo" src={Logo} alt="F8" />
          </Link>
          <h1 className="Login_title">Đăng nhập vào LV Education</h1>
        </div>

        <div className="Login_body">
          <div className="Login_mainStep">
            <Formik
              initialValues={{
                email: "",
                password: "",
              }}
              onSubmit={handleSubmit}
              validationSchema={valid}
            >
              {({ errors, touched, values }) => (
                <Form className="add-form">
                  <div className="SigninButton_wrapper">
                    <IconButton className="SigninButton_icon">
                      <HiOutlineMail />
                    </IconButton>
                    <Field
                      id="email"
                      name="email"
                      placeholder="Nhập Email của bạn..."
                    />
                  </div>
                  <div className="SigninButton_wrapper">
                    <IconButton className="SigninButton_icon">
                      <RiLockPasswordLine />
                    </IconButton>
                    <Field
                      type="password"
                      id="password"
                      name="password"
                      placeholder="Nhập Password của bạn..."
                    />
                    {errors.password && touched.password ? (
                      <div style={{ color: "red", textAlign: "left" }}>
                        {errors.password}
                      </div>
                    ) : null}
                  </div>

                  <Button
                    sx={{
                      display: "inline-block",
                      width: "50%",
                      margin: "22px 0",
                      borderRadius: "30px",
                    }}
                    variant="contained"
                    size="large"
                    type="submit"
                    disabled={values.email && values.password ? false : true}
                  >
                    Đăng nhập
                  </Button>
                </Form>
              )}
            </Formik>
          </div>
          <p className="Login_dontHaveAcc">
            Bạn chưa có tài khoản?
            <Button onClick={handleClick}> Đăng ký</Button>
          </p>
        </div>
      </div>

      <div className="Login_about">
        <Link to="/about-us" target="_blank" rel="noopener noreferrer">
          Giới thiệu về LV Edu
        </Link>
        <span>|</span>
        <Link
          to="https://url.mycv.vn/?ref=f8_register&amp;to=f8_youtube"
          target="_blank"
          rel="noopener noreferrer"
        >
          LV Edu trên Youtube
        </Link>
        <span>|</span>
        <Link
          to="https://url.mycv.vn/?ref=f8_register&amp;to=f8_fb_group"
          target="_blank"
          rel="noopener noreferrer"
        >
          LV Edu trên Facebook
        </Link>
      </div>
    </div>
  );
};

export default Login;
