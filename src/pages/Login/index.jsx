import React, { useState } from "react";
import Login from "./Login";
import Register from "./Register";
import "./style.scss";
const Index = () => {
  const [login, setLogin] = useState(true);
  const handleClick = () => {
    setLogin(!login);
  };
  return (
    <div className="Login_wrapper Login">
      {login ? (
        <Login handleClick={handleClick} />
      ) : (
        <Register handleClick={handleClick} />
      )}
    </div>
  );
};

export { Index };
