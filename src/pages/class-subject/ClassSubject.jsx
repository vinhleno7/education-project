import React, { useState } from "react";
import PropTypes from "prop-types";
import { dataListSubject } from "../../Mock/data-classlist";
import dataClassList from "../../Mock/data-classlist";
import "./class-subject.scss";
import { Link, NavLink, useNavigate } from "react-router-dom";
import { Formik, Field, Form } from "formik";
import { Button, Typography } from "@mui/material";
const ClassSubject = (props) => {
  let navigate = useNavigate();
  const [data, setData] = useState(dataListSubject);
  const handleClick = (key) => {
    localStorage.setItem("idSub", key);
    // navigate(`/class-list`);
  };

  const handleUpdateCS = () => {
    console.log("UPDATE CS");
  };
  const handleDeleteCS = () => {
    console.log("DELETE CS");
  };

  const [turnAdd, setTurnAdd] = useState(false);
  const handleTurnAdd = () => {
    setTurnAdd(!turnAdd);
  };

  return (
    <div className="subject">
      <h1 className="subject__title">Danh sách các môn học</h1>
      <Button variant="contained" size="medium" onClick={handleTurnAdd}>
        Thêm môn học
      </Button>
      <div className={turnAdd ? "new-sub turn" : "new-sub"}>
        <Typography component="h2" className="add-title" variant="h2">
          Nhập thông tin môn học{" "}
        </Typography>
        <Formik
          initialValues={{
            nameclass: "",
            image: ""
          }}
          onSubmit={async (values) => {
            await new Promise((r) => setTimeout(r, 500));
            alert(JSON.stringify(values, null, 2));
          }}
        >
          <Form className="add-form">
            <div className="add-subform">
              <label htmlFor="nameclass">Tên Môn Học</label>
              <Field id="nameclass" name="nameclass" placeholder="English" />
            </div>
            <div className="add-subform">
              <label htmlFor="image">Chọn Ảnh</label>
              <Field id="image" name="image" type="file" />
            </div>
            <div className="add-action">
              <Button variant="contained" size="large" type="submit">
                Thêm mới
              </Button>
              <Button
                variant="contained"
                size="large"
                color="error"
                onClick={handleTurnAdd}
              >
                Hủy
              </Button>
            </div>
          </Form>
        </Formik>
      </div>
      <div className="subject__list">
        {data.map((item, i) => (
          <div key={i} className="subject__item">
            <Link to="/class-list" onClick={() => handleClick(item.id)}>
              <div>
                <img style={{ width: "100%" }} src={item.image} alt="" />
                <h3>{item.name}</h3>
              </div>
            </Link>
            <div className="subject__item__action">
              {/* <Button variant="contained" size="medium">
                Thêm
              </Button> */}

              <Button
                variant="contained"
                size="medium"
                onClick={handleUpdateCS}
              >
                Sửa
              </Button>
              <Button
                variant="contained"
                size="medium"
                color="warning"
                onClick={handleDeleteCS}
              >
                Xóa
              </Button>
            </div>
          </div>
        ))}
      </div>
    </div>
  );
};

ClassSubject.propTypes = {};

export default ClassSubject;
