import React from "react";
import IconButton from "@mui/material/IconButton";
import MoreHorizIcon from "@mui/icons-material/MoreHoriz";
import VideoCallIcon from "@mui/icons-material/VideoCall";
import RateReviewIcon from "@mui/icons-material/RateReview";
import InputBase from "@mui/material/InputBase";
import SearchIcon from "@mui/icons-material/Search";
import DirectionsIcon from "@mui/icons-material/Directions";

import { NavLink } from "react-router-dom";
import "./style.scss";
import Header from "../../components/Header/Header";
import logo from "../../assets/img/logo/logo.png";
import user from "../../Mock/user";

function Messenger() {
  const contactUser = user.filter((person) => person.name === "Vân Anh");
  const MyMess = user.filter((person) => person.isMe === true);

  return (
    <div>
      {/* <Header /> */}
      <div className="chat_container">
        <div className="chat_user">
          <div className="chat_action">
            <div className="chat_action__title">
              <NavLink to="/">
                <img src={logo} alt="logo" />
              </NavLink>
            </div>
            <div className="chat_action__icon">
              <IconButton
                className="btnChatAction"
                color="secondary"
                aria-label="add an alarm"
              >
                <MoreHorizIcon className="icon_action" fontSize="large" />
              </IconButton>

              <IconButton
                className="btnChatAction"
                color="secondary"
                aria-label="add an alarm"
              >
                <VideoCallIcon className="icon_action" fontSize="large" />
              </IconButton>

              <IconButton
                className="btnChatAction"
                color="secondary"
                aria-label="add an alarm"
              >
                <RateReviewIcon className="icon_action" fontSize="large" />
              </IconButton>
            </div>
          </div>
          <div className="chat_search">
            {/* <input type="text" placeholder="Find ...." /> */}
            <IconButton
              type="submit"
              sx={{ p: " 8px 12px 8px 12px" }}
              aria-label="search"
            >
              <SearchIcon />
            </IconButton>
            <InputBase
              className="btnSearch"
              sx={{ ml: 1, flex: 1 }}
              placeholder="Search..."
              inputProps={{ "aria-label": "Search" }}
            />
          </div>
          <div className="chat_listUser">
            {user.map((person, index) => {
              return (
                <div key={index} className="chat_listUser__item">
                  <img src={person.image} alt="avatar" />
                  <div className="chat_person__info">
                    <h3>{person.name}</h3>
                    <p>Bạn: You are number 1 !!!</p>
                  </div>
                </div>
              );
            })}
          </div>
        </div>
        <div className="chat_content">
          <div className="chat_content__tittle">
            <div className="user_info">
              <img
                src="https://scontent.fsgn8-1.fna.fbcdn.net/v/t1.6435-9/91393919_656745315151426_534799054136147968_n.jpg?_nc_cat=111&ccb=1-5&_nc_sid=174925&_nc_ohc=hv3N-1eCRyIAX8SVw26&_nc_ht=scontent.fsgn8-1.fna&oh=00_AT-vl2EhiVObu7JrO79EcMgjE7fDeNRm2qFjqzcMiaErig&oe=626775E9"
                alt=""
              />

              {contactUser.map((person) => {
                return (
                  <div key={person.id} className="user_name">
                    <h1>{person.name}</h1>
                    <p>
                      {person.isActive
                        ? "Đang hoạt động"
                        : "Đã ngừng hoạt động"}
                    </p>
                  </div>
                );
              })}
            </div>

            <div className="user_action">
              <IconButton
                className="btnChatAction"
                color="secondary"
                aria-label="add an alarm"
              >
                <MoreHorizIcon className="icon_action" fontSize="large" />
              </IconButton>

              <IconButton
                className="btnChatAction"
                color="secondary"
                aria-label="add an alarm"
              >
                <VideoCallIcon className="icon_action" fontSize="large" />
              </IconButton>

              <IconButton
                className="btnChatAction"
                color="secondary"
                aria-label="add an alarm"
              >
                <RateReviewIcon className="icon_action" fontSize="large" />
              </IconButton>
            </div>
          </div>
          <div className="chat_content__message">
            <div className="message you">
              <span>Nguyen Thi Van Anh</span>
            </div>
            <div className="message me">
              {/* {
                MyMess.map((my) => {
                  return ()
                })
              } */}
              <span>Nguyen Huu Le Vinh</span>
            </div>
          </div>
          <div className="chat_content__input">
            <InputBase
              className="inputChat"
              placeholder="Typing..."
              inputProps={{ "aria-label": "Chat" }}
            />
            <IconButton
              color="primary"
              sx={{ p: "10px", fontSize: 25 }}
              aria-label="directions"
            >
              <DirectionsIcon />
            </IconButton>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Messenger;
