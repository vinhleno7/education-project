import React, { useState } from "react";
import { Link } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";
import { IoMdMore } from "react-icons/io";
import { GrSend } from "react-icons/gr";
import { IconButton, Menu, MenuItem } from "@mui/material";
import { deleteComment } from "../../redux/actions/commentAction";

const CommentMenu = ({ post, comment, auth }) => {
  const MenuItem2 = () => {
    return (
      <div>
        {/* <div className="dropdown-item">
          <span className="material-icons">create</span> Edit
        </div> */}
        <div
          style={{ cursor: "pointer" }}
          className="dropdown-item"
          onClick={handleRemove}
        >
          <span className="material-icons">delete_outline</span> Xóa
        </div>
      </div>
    );
  };
  const dispatch = useDispatch();
  // Dialog
  const handleRemove = () => {
    if (
      post.author._id === auth.user._id ||
      comment.author._id === auth.user._id
    ) {
      dispatch(deleteComment({ post, auth, comment }));
    }
  };
  const [openDialog, setOpenDialog] = useState(false);

  const handleClickOpenDialog = () => {
    setOpenDialog(true);
  };

  const handleCloseDialog = () => {
    setOpenDialog(false);
  };

  const [anchorEl, setAnchorEl] = React.useState(null);
  const [content, setContent] = useState("");
  const [onEdit, setOnEdit] = useState(false);

  const open = Boolean(anchorEl);
  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };
  return (
    <div>
      <IconButton
        onClick={handleClick}
        aria-controls={open ? "account-menu" : undefined}
        aria-haspopup="true"
        aria-expanded={open ? "true" : undefined}
        color="primary"
        size="large"
        aria-label=""
      >
        <IoMdMore className="post-list__right__icon" />
      </IconButton>
      <Menu
        anchorEl={anchorEl}
        id="account-menu"
        open={open}
        onClose={handleClose}
        onClick={handleClose}
        PaperProps={{
          elevation: 0,
          sx: {
            overflow: "visible",
            filter: "drop-shadow(0px 2px 8px rgba(0,0,0,0.32))",
            mt: 1.5,
            "& .MuiAvatar-root": {
              width: 32,
              height: 32,
              ml: -0.5,
              mr: 1,
              boxShadow:
                "rgba(0, 0, 0, 0.16) 0px 10px 36px 0px, rgba(0, 0, 0, 0.06) 0px 0px 0px 1px"
            },
            "&:before": {
              content: '""',
              display: "block",
              position: "absolute",
              top: 0,
              right: 14,
              width: 10,
              height: 10,
              bgcolor: "background.paper",
              transform: "translateY(-50%) rotate(45deg)",

              zIndex: 0
            }
          }
        }}
        transformOrigin={{ horizontal: "right", vertical: "top" }}
        anchorOrigin={{ horizontal: "right", vertical: "bottom" }}
      >
        {post.author._id === auth.user._id ? (
          comment.author._id === auth.user._id ? (
            MenuItem2()
          ) : (
            <div
              style={{ cursor: "pointer" }}
              className="dropdown-item"
              onClick={handleRemove}
            >
              <span className="material-icons">delete_outline</span> Xóa
            </div>
          )
        ) : (
          comment.author._id === auth.user._id && MenuItem2()
        )}
      </Menu>
    </div>
  );
};

export default CommentMenu;
