import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import listMember from "../../Mock/listmember";
import avatardefault from "../../assets/img/avatar/avatar-default.png";
import Button from "@mui/material/Button";
import { IconButton } from "@mui/material";
import DeleteIcon from "@mui/icons-material/Delete";
import { Formik, Field, Form } from "formik";
import { Typography } from "@mui/material";
import { useSelector, useDispatch } from "react-redux";
import { getAllUser } from "../../redux/actions/userAction";
import {
  addStudentCS,
  deleteStudentCS
} from "../../redux/actions/classSubjectAction";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

import "./class-detail.scss";

// MUI MULTIPLE SELECT
import Checkbox from "@mui/material/Checkbox";
import TextField from "@mui/material/TextField";
import Autocomplete from "@mui/material/Autocomplete";
import CheckBoxOutlineBlankIcon from "@mui/icons-material/CheckBoxOutlineBlank";
import CheckBoxIcon from "@mui/icons-material/CheckBox";
// Dialog
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogContentText from "@mui/material/DialogContentText";
import DialogTitle from "@mui/material/DialogTitle";

const icon = <CheckBoxOutlineBlankIcon fontSize="small" />;
const checkedIcon = <CheckBoxIcon fontSize="small" />;

toast.configure();
let myFinallyResult = [];
const ListMember = ({ myCS }) => {
  // Dialog
  const { auth, user, group } = useSelector((state) => state);
  const dispatch = useDispatch();
  const [myStudents, setMyStudents] = useState([]);

  const [openDialog, setOpenDialog] = useState(false);

  const handleClickOpenDialog = () => {
    setOpenDialog(true);
  };

  const handleCloseDialog = () => {
    setOpenDialog(false);
  };

  useEffect(() => {
    dispatch(getAllUser(auth));
  }, [dispatch, auth]);

  const [turnAddStudent, setTurnAddStudent] = useState(false);
  const handleTurnAddStudent = () => {
    setTurnAddStudent(true);

    // loc ra thanh vien phu hop de them
    let arrGr = [];
    for (const item of myCS.students) {
      arrGr.push(item);
    }
    let arrMembersExact = [];
    for (const v2 of user) {
      arrMembersExact.push(v2);
    }

    const B = arrGr;

    const A = arrMembersExact;

    // Lọc ra các thành viên chưa có nhóm
    myFinallyResult = A.filter((a) => !B.map((b) => b._id).includes(a._id));

    // - giao vien trong CS do
    myFinallyResult = myFinallyResult.filter(
      (item) => item._id !== myCS.teacher._id
    );
  };
  const handleOffAddStudent = () => {
    setTurnAddStudent(false);
  };

  const [listMembers, setListMember] = useState(listMember);

  const handleAddStudentList = (values) => {
    const classSubjectID = myCS._id;
    let studentID = [];
    for (const v of myStudents) {
      studentID.push(v._id);
    }
    dispatch(addStudentCS({ auth, classSubjectID, studentID }));
    if (studentID.length > 0) {
      handleOffAddStudent();
    }
  };

  const [idDel, setIdDel] = useState("");
  const [nameDel, setNameDel] = useState("");

  const dialogDelete = (id, name) => {
    setIdDel(id);
    setNameDel(name);
    handleClickOpenDialog();
  };
  const handleDeleteStudent = (idDel) => {
    const classSubjectID = myCS._id;
    const studentID = idDel;
    dispatch(deleteStudentCS({ auth, classSubjectID, studentID }));
    handleCloseDialog();
  };
  return (
    <div className="list-member">
      {myCS.teacher && (
        <div className="teacher">
          <h1>Giáo viên</h1>
          <div className="teacher__content">
            <img src={myCS.teacher.avatar} alt="" />
            <span>{myCS.teacher.fullName}</span>
          </div>
        </div>
      )}
      <div className="member">
        <div className="member__header">
          <h2>Bạn học</h2>
          {auth.user.role === "teacher" && (
            <Button
              variant="contained"
              size="medium"
              style={{ margin: "4px 20%" }}
              onClick={handleTurnAddStudent}
            >
              Thêm thành viên
            </Button>
          )}
          <div
            className={
              turnAddStudent ? "new-Student turnStudent" : "new-Student"
            }
          >
            <Typography component="h2" className="add-title" variant="h2">
              Nhập thông tin sinh viên cần thêm{" "}
            </Typography>
            <Formik
              initialValues={{
                mssv: ""
              }}
              onSubmit={async (values) => {
                // await new Promise((r) => setTimeout(r, 500));
                // alert(JSON.stringify(values, null, 2));
                handleAddStudentList(values);
              }}
            >
              <Form className="add-form">
                <div className="add-subform">
                  <label htmlFor="mssv">Sinh viên</label>
                  <Autocomplete
                    multiple
                    id="checkboxes-tags-demo"
                    onChange={(event, value) => setMyStudents(value)}
                    options={myFinallyResult}
                    noOptionsText="Không có người dùng nào trên hệ thống"
                    disableCloseOnSelect
                    getOptionLabel={(option) => option.fullName}
                    renderOption={(props, option, { selected }) => (
                      <li {...props}>
                        <Checkbox
                          icon={icon}
                          checkedIcon={checkedIcon}
                          style={{ marginRight: 8 }}
                          checked={selected}
                        />
                        {option.fullName}
                      </li>
                    )}
                    style={{ width: 400, marginBottom: "30px" }}
                    renderInput={(params) => (
                      <TextField
                        {...params}
                        label="Sinh viên"
                        placeholder="Chọn sinh viên mà bạn muốn thêm vào group"
                      />
                    )}
                  />
                </div>
                <div className="add-action">
                  <Button variant="contained" size="large" type="submit">
                    Thêm mới
                  </Button>
                  <Button
                    variant="contained"
                    size="large"
                    color="error"
                    onClick={handleOffAddStudent}
                  >
                    Hủy
                  </Button>
                </div>
              </Form>
            </Formik>
          </div>
          {myCS && <span>{myCS.students.length} sinh viên</span>}
        </div>
        {myCS.students.map((item, i) => (
          <div className="member__content" key={i}>
            <img src={item.avatar} alt="" />
            <span>{item.fullName}</span>
            {auth.user.role === "teacher" && (
              <IconButton
                aria-label="delete"
                size="large"
                color="error"
                onClick={() => dialogDelete(item._id, item.fullName)}
              >
                <DeleteIcon fontSize="medium" />
              </IconButton>
            )}
          </div>
        ))}
      </div>
      <Dialog
        open={openDialog}
        onClose={handleCloseDialog}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">{"XÓA SINH VIÊN !"}</DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            Bạn có muốn xóa sinh viên{" "}
            <span style={{ color: "red", fontStyle: "italic" }}>{nameDel}</span>{" "}
            không ?
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleCloseDialog}>Hủy</Button>
          <Button
            variant="contained"
            color="error"
            onClick={() => handleDeleteStudent(idDel)}
            autoFocus
          >
            Xóa
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
};

ListMember.propTypes = {};

export default ListMember;
