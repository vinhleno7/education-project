import {
  Button,
  Dialog,
  IconButton,
  Menu,
  MenuItem,
  Typography,
} from "@mui/material";
import React, { useState } from "react";
import img from "../../assets/img/bg/p1.jpg";
import "./class-detail.scss";
import MoreVertIcon from "@mui/icons-material/MoreVert";
import { Slide } from "react-toastify";
import UpdateClassSubject from "./UpdateClassSubject";
import { Field, Form, Formik } from "formik";
import * as Yup from "yup";
import { useDispatch, useSelector } from "react-redux";
import { updateClassSubject } from "../../redux/actions/classSubjectAction";
const ClassDetailCard = ({ myCS }) => {
  const { auth } = useSelector((state) => state);
  const { _id } = myCS;
  const id = _id;
  const [anchorEl, setAnchorEl] = useState(null);
  const open = Boolean(anchorEl);
  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };
  // update classSubject model
  const [openModel, setOpenModel] = useState(false);

  const handleClickOpenModel = () => {
    setOpenModel(true);
  };

  const handleClickCloseModel = () => {
    setOpenModel(false);
  };
  const valid = Yup.object({
    nameclasssubject: Yup.string().required("No name provided."),
  });
  const dispatch = useDispatch();
  const handleSubmit = async (values) => {
    const newName = values.nameclasssubject;
    dispatch(updateClassSubject({ newName, id, auth }));
  };
  return (
    <>
      <div className="ClassDetailCard">
        <img className="ClassDetailCard__img" src={img} alt="" />
        <div className="ClassDetailCard__content">
          <h2>{myCS.name}</h2>
          {auth.user.role === "teacher" ? (
            <IconButton
              color="primary"
              aria-label="Sửa tên lớp học"
              onClick={handleClick}
            >
              <MoreVertIcon className="ClassDetailCard__content__icon" />
            </IconButton>
          ) : null}
        </div>
      </div>
      <Menu
        anchorEl={anchorEl}
        id="account-menu"
        open={open}
        onClose={handleClose}
        onClick={handleClose}
        PaperProps={{
          elevation: 0,
          sx: {
            overflow: "visible",
            filter: "drop-shadow(0px 2px 8px rgba(0,0,0,0.32))",
            mt: 1.5,
            "& .MuiAvatar-root": {
              width: 32,
              height: 32,
              ml: -0.5,
              mr: 1,
              boxShadow:
                "rgba(0, 0, 0, 0.16) 0px 10px 36px 0px, rgba(0, 0, 0, 0.06) 0px 0px 0px 1px",
            },
            "&:before": {
              content: '""',
              display: "block",
              position: "absolute",
              top: 0,
              right: 14,
              width: 10,
              height: 10,
              bgcolor: "background.paper",
              transform: "translateY(-50%) rotate(45deg)",

              zIndex: 0,
            },
          },
        }}
        transformOrigin={{ horizontal: "right", vertical: "top" }}
        anchorOrigin={{ horizontal: "right", vertical: "bottom" }}
      >
        <MenuItem onClick={handleClickOpenModel} sx={{ fontSize: "15px" }}>
          Sửa
        </MenuItem>
        <MenuItem sx={{ fontSize: "15px" }}>Sao chép link lớp học</MenuItem>
      </Menu>
      <Dialog
        open={openModel}
        onClose={handleClickCloseModel}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <div style={{ padding: "20px " }}>
          <Typography component="h2" className="add-title" variant="h2">
            Nhập thông tin lớp môn học{" "}
          </Typography>
          <h1 style={{ paddingTop: "20px" }}>
            Tên Nhóm Lớp Cũ :{" "}
            <span style={{ color: "rgb(241, 110, 52)" }}>{myCS.name}</span>
          </h1>
          <Formik
            initialValues={{
              nameclasssubject: "",
            }}
            validationSchema={valid}
            onSubmit={handleSubmit}
          >
            <Form className="add-form">
              <div className="add-subform">
                <label htmlFor="className">Tên Lớp Học</label>
                <Field id="nameclasssubject" name="nameclasssubject" />
              </div>
              <div className="add-action">
                <Button variant="contained" size="large" type="submit">
                  Cập nhật
                </Button>
                <Button
                  variant="contained"
                  size="large"
                  color="error"
                  onClick={handleClickCloseModel}
                >
                  Hủy
                </Button>
              </div>
            </Form>
          </Formik>
        </div>
      </Dialog>
    </>
  );
};

export default ClassDetailCard;
