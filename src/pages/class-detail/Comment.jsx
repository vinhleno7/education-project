import React, { useState } from "react";
import { Link } from "react-router-dom";
import CommentMenu from "./CommentMenu";
import { useSelector, useDispatch } from "react-redux";
import { IoMdMore } from "react-icons/io";
import { GrSend } from "react-icons/gr";
import { IconButton, Menu, MenuItem } from "@mui/material";
import moment from "moment";

const Comment = ({ comment, post }) => {
  // const styleCard = {
  //   opacity: comment._id ? 1 : 0.5,
  //   pointerEvents: comment._id ? "inherit" : "none"
  // };

  // Dialog

  const [openDialog, setOpenDialog] = useState(false);

  const handleClickOpenDialog = () => {
    setOpenDialog(true);
  };

  const handleCloseDialog = () => {
    setOpenDialog(false);
  };

  const [anchorEl, setAnchorEl] = React.useState(null);
  const [content, setContent] = useState("");
  const [onEdit, setOnEdit] = useState(false);

  const { auth } = useSelector((state) => state);
  const dispatch = useDispatch();

  const open = Boolean(anchorEl);
  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };

  const MenuItem2 = () => {
    return (
      <>
        <MenuItem onClick={console.log("xóa")}>Xóa</MenuItem>

        <MenuItem onClick={console.log("Sua")}>Sửa</MenuItem>
      </>
    );
  };

  return (
    <>
      {comment.length > 0 &&
        comment.map((item) => (
          <div
            key={item._id}
            className="comment_card mt-2"
            // style={styleCard}
          >
            <div
              to="/"
              // {`/profile/${comment.user._id}`}
              className="d-flex text-dark"
            >
              {/* <img
                src="https://www.google.com/url?sa=i&url=https%3A%2F%2Fsuno.vn%2Fblog%2F4-yeu-cho-mot-logo-hoan-hao%2F&psig=AOvVaw0gsaW14Bot2OnmWOI2Y4i8&ust=1651287850957000&source=images&cd=vfe&ved=0CAwQjRxqFwoTCJDZqt-kuPcCFQAAAAAdAAAAABAO"
                alt=""
              {/* /> */}
              {/* <Avatar
          src="https://www.google.com/url?sa=i&url=https%3A%2F%2Fsuno.vn%2Fblog%2F4-yeu-cho-mot-logo-hoan-hao%2F&psig=AOvVaw0gsaW14Bot2OnmWOI2Y4i8&ust=1651287850957000&source=images&cd=vfe&ved=0CAwQjRxqFwoTCJDZqt-kuPcCFQAAAAAdAAAAABAO"
          size="small-avatar"
        /> */}
              {item.author && (
                <h6 style={{ fontWeight: "bolder" }} className="mx-1">
                  {item.author.fullName}
                </h6>
              )}
            </div>

            <div className="comment_content">
              <div className="flex-fill">
                {/* {onEdit ? (
            <textarea
              rows="5"
              value={content}
              onChange={(e) => setContent(e.target.value)}
            />
          ) : (
            <div>
              <span>
                {content.length < 100
                  ? content
                  : readMore
                  ? content + " "
                  : content.slice(0, 100) + "..."}
              </span>
              {content.length > 100 && (
                <span
                  className="readMore"
                  onClick={() => setReadMore(!readMore)}
                >
                  {readMore ? "Hide content" : "Read more"}
                </span>
              )}
            </div>
          )} */}
                {item.content}
                {/* <CommentMenu
                  post={post}
                  comment={item}
                  auth={auth}
                  // setOnEdit={setOnEdit}
                /> */}
              </div>
              <span> {moment(item.createdAt).fromNow()}</span>

              <div
                className="d-flex align-items-center mx-2"
                style={{ cursor: "pointer" }}
              >
                <CommentMenu post={post} comment={item} auth={auth} />

                {/* <LikeButton
                  isLike={isLike}
                  handleLike={handleLike}
                  handleUnLike={handleUnLike}
                /> */}
              </div>
            </div>
          </div>
        ))}
    </>
  );
};

export default Comment;
