import { Button, Typography } from "@mui/material";
import { Field, Form, Formik } from "formik";
import React, { useEffect, useState } from "react";
import "../class-list/class-list.scss";
import * as Yup from "yup";
import { toast } from "react-toastify";
import { useDispatch, useSelector } from "react-redux";
import axios from "axios";
import {
  createClassSubject,
  updateClassSubject,
} from "../../redux/actions/classSubjectAction";
toast.configure();
const CreateClass = ({ handleClickCloseModel, myCS }) => {
  const { auth } = useSelector((state) => state);
  const { _id } = myCS;
  const id = _id;
  console.log("id", id);
  const valid = Yup.object({
    nameclasssubject: Yup.string().required("No name provided."),
  });
  const dispatch = useDispatch();
  const handleSubmit = async (values) => {
    const nameclasssubject = values.nameclasssubject;
    let dataUpdateCS = {
      name: nameclasssubject,
    };
    console.log("Data", dataUpdateCS);
  };
  return (
    <div style={{ padding: "20px " }}>
      <Typography component="h2" className="add-title" variant="h2">
        Nhập thông tin lớp môn học{" "}
      </Typography>
      <Formik
        initialValues={{
          nameclasssubject: "",
        }}
        validationSchema={valid}
        onSubmit={handleSubmit}
      >
        <Form className="add-form">
          <div className="add-subform">
            <label htmlFor="className">Tên Lớp Học</label>
            <Field
              id="nameclasssubject"
              name="nameclasssubject"
              value={myCS.name}
            />
          </div>
          <div className="add-action">
            <Button variant="contained" size="large" type="submit">
              Thêm mới
            </Button>
            <Button
              variant="contained"
              size="large"
              color="error"
              onClick={handleClickCloseModel}
            >
              Hủy
            </Button>
          </div>
        </Form>
      </Formik>
    </div>
  );
};

export default CreateClass;
