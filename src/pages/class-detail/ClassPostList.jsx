import React, { useState } from "react";
import PropTypes from "prop-types";
import { IoMdMore } from "react-icons/io";
import { GrSend } from "react-icons/gr";
import avatardefault from "../../assets/img/avatar/avatar-default.png";
import { IconButton, Menu, MenuItem } from "@mui/material";
import "./class-detail.scss";
import { Field, Form, Formik } from "formik";
import moment from "moment";
import { useSelector, useDispatch } from "react-redux";
import { GLOBALTYPES } from "../../redux/actions/globalTypes";
import Button from "@mui/material/Button";
import { updatePost, deletePost } from "../../redux/actions/postAction";
import { createComment } from "../../redux/actions/commentAction";
import ArrowForwardIosIcon from "@mui/icons-material/ArrowForwardIos";
import SendRoundedIcon from "@mui/icons-material/SendRounded";
// Dialog
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogContentText from "@mui/material/DialogContentText";
import DialogTitle from "@mui/material/DialogTitle";

const ClassPostAssignment = ({ post }) => {
  // Dialog

  const [openDialog, setOpenDialog] = useState(false);

  const handleClickOpenDialog = () => {
    setOpenDialog(true);
  };

  const handleCloseDialog = () => {
    setOpenDialog(false);
  };

  const [anchorEl, setAnchorEl] = React.useState(null);
  const [content, setContent] = useState("");
  const [onEdit, setOnEdit] = useState(false);

  const { auth, status } = useSelector((state) => state);
  const dispatch = useDispatch();

  const open = Boolean(anchorEl);
  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };

  const handleSubmit = async (values) => {
    // await new Promise((r) => setTimeout(r, 500));
    // alert(JSON.stringify(values, null, 2));
    // if (!values.comment.trim()) return;

    const newComment = {
      content: values.comment,

      author: auth.user,
      createdAt: new Date().toISOString()
    };
    dispatch(createComment(post, newComment, auth));
    values.comment = "";
  };
  const handleEditPost = () => {
    dispatch({
      type: GLOBALTYPES.STATUS,
      payload: { ...post, onEdit: true }
    });
    setContent(post.content);
    setOnEdit(true);
  };
  const handleUpdate = (e) => {
    e.preventDefault();

    if (content !== post.content) {
      dispatch(updatePost({ content, auth, status }));

      setOnEdit(false);
    } else {
      setOnEdit(false);
    }
  };

  const dialogDelete = () => {
    handleClickOpenDialog();
  };
  const handleDeletePost = () => {
    dispatch(deletePost({ post, auth }));

    handleCloseDialog();
  };

  return (
    <>
      <div className="post-list">
        <div className="post-list__left">
          <div className="post-list__left__img">
            <img src={post.author.avatar} alt="" />
          </div>
          <div className="post-list__left__content">
            <h3>
              {post.author._id === auth.user._id ? "Tôi" : post.author.fullName}{" "}
              {post.group ? (
                <span>
                  <SendRoundedIcon /> {post.group.name.toUpperCase()}
                </span>
              ) : post.partner ? (
                <span>
                  <SendRoundedIcon />{" "}
                  {post.partner._id === auth.user._id
                    ? "Tôi"
                    : post.partner.fullName.toUpperCase()}
                </span>
              ) : (
                ""
              )}
            </h3>
            <span> {moment(post.createdAt).fromNow()}</span>
          </div>
        </div>

        <div className="post-list__right">
          <IconButton
            onClick={handleClick}
            aria-controls={open ? "account-menu" : undefined}
            aria-haspopup="true"
            aria-expanded={open ? "true" : undefined}
            color="primary"
            size="large"
            aria-label=""
          >
            <IoMdMore className="post-list__right__icon" />
          </IconButton>
        </div>
      </div>
      <div className="render-post">
        <span>{post.content}</span>

        {onEdit && (
          <div
            // onSubmit={handleUpdate}
            style={{ padding: "20px ", display: "block" }}
          >
            <textarea
              className="inputUpdate"
              name="content"
              value={content}
              // placeholder={`Đăng bài thảo luận đi nào, ${myAuth.fullName}...`}
              onChange={(e) => setContent(e.target.value)}
              style={{ margin: "8px" }}
            />
            <div>
              <Button
                className="btnUpdate"
                onClick={handleUpdate}
                variant="contained"
                style={{ width: "20%" }}
              >
                Đăng
              </Button>
            </div>
          </div>
        )}
      </div>
      <div className="comment-post">
        <div className="comment-post__avatar">
          <img src={auth.user.avatar} alt="" />
        </div>
        <div className="comment-post__box-input">
          <Formik initialValues={{ comment: "" }} onSubmit={handleSubmit}>
            <Form method="post">
              <Field
                type="text"
                placeholder="Phản hồi bài thảo luận ..."
                name="comment"
              />
              <button type="submit">
                <GrSend className="comment-post__box-input__icon" />
              </button>
            </Form>
          </Formik>
        </div>
      </div>
      <Menu
        anchorEl={anchorEl}
        id="account-menu"
        open={open}
        onClose={handleClose}
        onClick={handleClose}
        PaperProps={{
          elevation: 0,
          sx: {
            overflow: "visible",
            filter: "drop-shadow(0px 2px 8px rgba(0,0,0,0.32))",
            mt: 1.5,
            "& .MuiAvatar-root": {
              width: 32,
              height: 32,
              ml: -0.5,
              mr: 1,
              boxShadow:
                "rgba(0, 0, 0, 0.16) 0px 10px 36px 0px, rgba(0, 0, 0, 0.06) 0px 0px 0px 1px"
            },
            "&:before": {
              content: '""',
              display: "block",
              position: "absolute",
              top: 0,
              right: 14,
              width: 10,
              height: 10,
              bgcolor: "background.paper",
              transform: "translateY(-50%) rotate(45deg)",

              zIndex: 0
            }
          }
        }}
        transformOrigin={{ horizontal: "right", vertical: "top" }}
        anchorOrigin={{ horizontal: "right", vertical: "bottom" }}
      >
        {auth.user._id === post.author._id && (
          <div>
            <MenuItem sx={{ fontSize: "15px" }} onClick={handleEditPost}>
              Chỉnh sửa bài thảo luận
            </MenuItem>
            <MenuItem sx={{ fontSize: "15px" }} onClick={() => dialogDelete()}>
              Xóa bài thảo luận
            </MenuItem>
          </div>
        )}
        <MenuItem sx={{ fontSize: "15px" }}>
          Sao chép đường dẫn bài thảo luận
        </MenuItem>
      </Menu>
      <Dialog
        open={openDialog}
        onClose={handleCloseDialog}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">{"XÓA SINH VIÊN !"}</DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            <h3 style={{ color: "red" }}>
              Bạn có muốn xóa bài thảo luận này không ?
            </h3>
            <h4
              style={{
                fontStyle: "italic",
                paddingLeft: "4px",
                textAlign: "center"
              }}
            >
              {post.content}
            </h4>
            <p style={{ textAlign: "end" }}> By: {post.author.fullName}</p>
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleCloseDialog}>Hủy</Button>
          <Button
            variant="contained"
            color="error"
            onClick={() => handleDeletePost()}
            autoFocus
          >
            Xóa
          </Button>
        </DialogActions>
      </Dialog>
    </>
  );
};

ClassPostAssignment.propTypes = {};

export default ClassPostAssignment;
