import React, { useState } from "react";
import PropTypes from "prop-types";
import "./class-detail.scss";
import { BsArrowRightCircle } from "react-icons/bs";
import { Container, IconButton } from "@mui/material";
import avatardefault from "../../assets/img/avatar/avatar-default.png";
import group from "../../Mock/listgroup";
import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";
import Modal from "@mui/material/Modal";
import { Formik, Field, Form } from "formik";
import { GrAdd } from "react-icons/gr";
import SelectChipMember from "./SelectChipMember";
import logo from "../../assets/img/logo/logo.png";
import DeleteIcon from "@mui/icons-material/Delete";
import { useSelector, useDispatch } from "react-redux";
import {
  addMembers,
  deleteMember,
  updateGroup,
  deleteGroup
} from "../../redux/actions/groupAction";
// MUI MULTIPLE SELECT
import Checkbox from "@mui/material/Checkbox";
import TextField from "@mui/material/TextField";
import Autocomplete from "@mui/material/Autocomplete";
import CheckBoxOutlineBlankIcon from "@mui/icons-material/CheckBoxOutlineBlank";
import CheckBoxIcon from "@mui/icons-material/CheckBox";
// Dialog
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogContentText from "@mui/material/DialogContentText";
import DialogTitle from "@mui/material/DialogTitle";

import "react-toastify/dist/ReactToastify.css";

import { toast } from "react-toastify";
const icon = <CheckBoxOutlineBlankIcon fontSize="small" />;
const checkedIcon = <CheckBoxIcon fontSize="small" />;

toast.configure();

const style = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 800,
  bgcolor: "background.paper",
  border: "1px solid #ccc",
  boxShadow: 24,
  p: 4
};

const Group = ({ members, item }) => {
  const [openDialog, setOpenDialog] = useState(false);

  const handleClickOpenDialog = () => {
    setOpenDialog(true);
  };

  const handleCloseDialog = () => {
    setOpenDialog(false);
  };

  const [myStudents, setMyStudents] = useState([]);
  const { auth, itemCS } = useSelector((state) => state);

  const dispatch = useDispatch();

  const [turnDetailMember, setTurnDetailMember] = useState(false);
  const handleTurnDetailMember = () => {
    if (item.members.find((member) => member._id === auth.user._id)) {
      setTurnDetailMember(!turnDetailMember);
    } else if (auth.user._id === itemCS.teacher._id) {
      setTurnDetailMember(!turnDetailMember);
    } else {
      toast.error(`Bạn không thuộc nhóm ${item.name} nên không vào xem được.`);
    }
  };
  const [open, setOpen] = useState(false);
  const handleOpenAdd = () => setOpen(true);
  const handleCloseAdd = () => setOpen(false);

  // open modal updateNameGr
  const [openGr, setOpenGr] = useState(false);
  const handleOpenGrAdd = () => setOpenGr(true);
  const handleCloseGrAdd = () => setOpenGr(false);
  const [newName, setNewName] = useState(item.name);

  const handleDeleteMember = (id) => {
    const name = item.name;

    dispatch(deleteMember({ auth, name, id }));
  };

  const dialogDelete = () => {
    handleClickOpenDialog();
  };

  const handleDeleteGroup = (item) => {
    dispatch(deleteGroup({ auth, item }));
    handleTurnDetailMember();
  };

  return (
    <>
      <div
        className={turnDetailMember ? "group activeGroup" : "group"}
        onClick={handleTurnDetailMember}
      >
        <div className="group__header">
          <h3>{item.name}</h3>
          <IconButton>
            <BsArrowRightCircle className="group__header__icon" />
          </IconButton>
        </div>
        <div className="group__people">
          Số lượng thành viên ({item.members.length})
        </div>
        <div className="group__avatar">
          {item.members.map((student) => (
            <img
              key={student._id}
              src={student.avatar ? student.avatar : student}
              alt={student}
            />
          ))}
        </div>
      </div>
      <div
        className={
          turnDetailMember
            ? "detailMemberOfGroup turnDetail"
            : "detailMemberOfGroup"
        }
      >
        <div className="group__detail">
          <h1>Chi tiết nhóm học: </h1>
          <img src={logo} alt="This is logo group" />
          <h1>{item.name}</h1>

          <p>Số lượng({item.members.length})</p>
          <div>
            <Button
              variant="contained"
              color="error"
              size="medium"
              onClick={() => dialogDelete()}
              style={{ margin: "0 8px" }}
            >
              Xóa nhóm
            </Button>
            <Dialog
              open={openDialog}
              onClose={handleCloseDialog}
              aria-labelledby="alert-dialog-title"
              aria-describedby="alert-dialog-description"
            >
              <DialogTitle id="alert-dialog-title">{"XÓA NHÓM !"}</DialogTitle>
              <DialogContent>
                <DialogContentText id="alert-dialog-description">
                  <h3 style={{ color: "red" }}>
                    Bạn có muốn xóa nhóm {item.name} này không ?
                  </h3>
                  <h4
                    style={{
                      fontStyle: "italic",
                      paddingLeft: "4px",
                      textAlign: "center"
                    }}
                  >
                    {item.name}
                  </h4>
                  {/* <p style={{ textAlign: "end" }}>
                    {" "}
                    By: {post.author.fullName}
                  </p> */}
                </DialogContentText>
              </DialogContent>
              <DialogActions>
                <Button onClick={handleCloseDialog}>Hủy</Button>
                <Button
                  variant="contained"
                  color="error"
                  onClick={() => handleDeleteGroup(item)}
                  autoFocus
                >
                  Xóa
                </Button>
              </DialogActions>
            </Dialog>
            <Button variant="contained" size="medium" onClick={handleOpenGrAdd}>
              Đổi tên
            </Button>
          </div>
          <Modal
            open={openGr}
            onClose={handleCloseGrAdd}
            aria-labelledby="modal-modal-title"
            aria-describedby="modal-modal-description"
          >
            <Box sx={style}>
              <Typography
                id="modal-modal-title"
                variant="h6"
                component="h2"
                className="modal-title"
              >
                Sửa tên nhóm
              </Typography>
              <div id="modal-modal-description" sx={{ mt: 2 }}>
                <Formik
                  initialValues={{
                    namegroup: ""
                  }}
                  onSubmit={async (values) => {
                    dispatch(updateGroup({ auth, newName, item }));
                    console.log(
                      "🚀 ~ file: Group.jsx ~ line 209 ~ onSubmit={ ~ { auth, newName, item }",
                      { auth, newName, item }
                    );
                    handleCloseGrAdd();
                  }}
                >
                  <Form className="add-form">
                    <div className="add-subform modal-addgroup">
                      <label htmlFor="member">Tên nhóm</label>

                      <TextField
                        id="outlined-basic"
                        label={item.name}
                        onChange={(e) => setNewName(e.target.value)}
                        variant="outlined"
                      />
                    </div>
                    <div className="add-action">
                      <Button variant="contained" size="large" type="submit">
                        Sửa tên
                      </Button>
                      <Button
                        variant="contained"
                        size="large"
                        color="error"
                        onClick={handleCloseGrAdd}
                      >
                        Hủy
                      </Button>
                    </div>
                  </Form>
                </Formik>
              </div>
            </Box>
          </Modal>
        </div>

        <div className="group__members">
          <h1>Danh sách thành viên: </h1>

          {/* <div
              style={{ width: "40%", height: "1px", backgroundColor: "black" }}
            ></div> */}
          {item.members.map((student) => (
            <div className="item-member" key={student._id}>
              <img
                src={student.avatar ? student.avatar : student}
                alt={student}
              />
              <h2 className={student.isLeader ? "Leader" : ""}>
                {student.fullName}
              </h2>
              <IconButton
                aria-label="delete"
                size="large"
                color="error"
                onClick={() => handleDeleteMember(student._id)}
              >
                <DeleteIcon fontSize="medium" />
              </IconButton>
            </div>
          ))}
          <Button
            variant="contained"
            size="medium"
            style={{ margin: "4px 20%" }}
            onClick={handleOpenAdd}
          >
            Thêm thành viên vào nhóm
          </Button>

          <Modal
            open={open}
            onClose={handleCloseAdd}
            aria-labelledby="modal-modal-title"
            aria-describedby="modal-modal-description"
          >
            <Box sx={style}>
              <Typography
                id="modal-modal-title"
                variant="h6"
                component="h2"
                className="modal-title"
              >
                Thêm thành viên
              </Typography>
              <div id="modal-modal-description" sx={{ mt: 2 }}>
                <Formik
                  initialValues={{
                    namegroup: ""
                  }}
                  onSubmit={async (values) => {
                    values.namegroup = item.name;
                    dispatch(
                      addMembers({
                        auth,
                        values,
                        myStudents
                      })
                    );
                    handleCloseAdd();
                  }}
                >
                  <Form className="add-form">
                    <div className="add-subform modal-addgroup">
                      <label htmlFor="member">Sinh viên</label>
                      {/* MULTIPLE SELECT */}

                      <Autocomplete
                        multiple
                        id="checkboxes-tags-demo"
                        onChange={(event, value) => setMyStudents(value)}
                        options={members}
                        noOptionsText="Các nhóm đã đủ thành viên...."
                        disableCloseOnSelect
                        getOptionLabel={(option) => option.fullName}
                        renderOption={(props, option, { selected }) => (
                          <li {...props}>
                            <Checkbox
                              icon={icon}
                              checkedIcon={checkedIcon}
                              style={{ marginRight: 8 }}
                              checked={selected}
                            />
                            {option.fullName}
                          </li>
                        )}
                        style={{ width: 400, marginBottom: "30px" }}
                        renderInput={(params) => (
                          <TextField
                            {...params}
                            label="Sinh viên"
                            placeholder="Chọn sinh viên mà bạn muốn thêm vào group"
                          />
                        )}
                      />
                      {/* END */}
                    </div>
                    <div className="add-action">
                      <Button variant="contained" size="large" type="submit">
                        Thêm sinh viên
                      </Button>
                      <Button
                        variant="contained"
                        size="large"
                        color="error"
                        onClick={handleCloseAdd}
                      >
                        Hủy
                      </Button>
                    </div>
                  </Form>
                </Formik>
              </div>
            </Box>
          </Modal>
        </div>
        <Button
          variant="contained"
          color="error"
          size="medium"
          onClick={handleTurnDetailMember}
          className="btnClose"
        >
          Đóng
        </Button>
      </div>
    </>
  );
};

export default Group;
