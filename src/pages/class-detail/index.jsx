import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";
import Tabs from "@mui/material/Tabs";
import Tab from "@mui/material/Tab";
import Typography from "@mui/material/Typography";
import Box from "@mui/material/Box";
import Header from "../../components/Header/Header";
import ClassDetail from "./ClassDetail";
import ListGroup from "./ListGroup";
import ListMember from "./ListMember";
import Private from "./Private";
import "./class-detail.scss";
import { useSelector, useDispatch } from "react-redux";
import { getDetailCS } from "../../redux/actions/classSubjectAction";
import { useParams } from "react-router-dom";
import { getAllRequestGroup } from "../../redux/actions/requestChangeGroup";

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box sx={{ p: 3 }}>
          <div>{children}</div>
        </Box>
      )}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.number.isRequired,
  value: PropTypes.number.isRequired
};

function a11yProps(index) {
  return {
    id: `simple-tab-${index}`,
    "aria-controls": `simple-tabpanel-${index}`
  };
}
const Index = () => {
  const { auth, itemCS, requestGr } = useSelector((state) => state);

  const { id } = useParams();
  const [newCS, setNewCS] = useState([]);
  const [newAuthor, setNewAuthor] = useState([]);
  const dispatch = useDispatch();
  useEffect(() => {
    if (auth.token) {
      dispatch(getDetailCS(auth, id));

      dispatch(getAllRequestGroup(auth, id));

      // setNewCS(itemCS);
      setNewAuthor(auth.user);
    }
  }, [id, dispatch, auth.token]);

  const [value, setValue] = React.useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };
  return (
    <>
      <Header />
      {newCS && (
        <Box sx={{ width: "100%" }}>
          <Box sx={{ borderBottom: 1, borderColor: "divider" }}>
            <Tabs
              value={value}
              onChange={handleChange}
              aria-label="basic tabs example"
              style={{ outline: "none !important" }}
            >
              <Tab label="Bảng tin" {...a11yProps(0)} />
              <Tab label="Nhóm" {...a11yProps(1)} />
              <Tab label="Mọi người" {...a11yProps(2)} />
              {/* <Tab label="Yêu cầu và thảo luận riêng" {...a11yProps(3)} /> */}
            </Tabs>
          </Box>
          <TabPanel value={value} index={0}>
            <ClassDetail
              myRequestGr={requestGr}
              myAuth={newAuthor}
              myCS={itemCS}
            />
          </TabPanel>

          <TabPanel value={value} index={1}>
            <ListGroup myCS={itemCS} />
          </TabPanel>

          <TabPanel value={value} index={2}>
            <ListMember myCS={itemCS} />
          </TabPanel>

          <TabPanel value={value} index={3}>
            <Private myCS={itemCS} />
          </TabPanel>
        </Box>
      )}
    </>
  );
};

export default Index;
