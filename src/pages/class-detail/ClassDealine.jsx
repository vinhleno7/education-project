import React from "react";
import PropTypes from "prop-types";
import { Button } from "@mui/material";
import "./class-detail.scss";
const ClassDealine = (props) => {
  return (
    <>
      <div className="class-dealine">
        <h3>Sắp đến hạn</h3>
        <span>Tuyệt vời, không có bài tập nào sắp đến hạn!</span>
      </div>
      <div className="class-btn">
        <Button variant="outlined">Xem tất cả</Button>
      </div>
    </>
  );
};

ClassDealine.propTypes = {};

export default ClassDealine;
