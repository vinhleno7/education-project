import React, { useEffect, useState } from "react";
import avatardefault from "../../assets/img/avatar/avatar-default.png";
import { CKEditor } from "@ckeditor/ckeditor5-react";
import ClassicEditor from "@ckeditor/ckeditor5-build-classic";
import { Link } from "react-router-dom";
import "./class-detail.scss";
import { Button, IconButton } from "@mui/material";
import { FaGoogleDrive } from "react-icons/fa";
import { AiOutlineUpload } from "react-icons/ai";
import { useDispatch, useSelector } from "react-redux";
import { createPost } from "../../redux/actions/postAction";
import TextField from "@mui/material/TextField";
import Autocomplete from "@mui/material/Autocomplete";

const ClassPost = ({ myAuth, myCS }) => {
  const { auth, group } = useSelector((state) => state);
  const dispatch = useDispatch();
  const [content, setContent] = useState("");

  const [open, setOpen] = useState(false);
  const handleClick = () => {
    setOpen(!open);
  };
  const handleCreatePost = () => {
    console.log("Create post");
    setOpen(!open);
  };
  const handleSubmit = (e) => {
    e.preventDefault();
    dispatch(createPost({ content, auth, myCS }));
    setContent("");
  };

  let myGroup;
  for (const item of group.groups) {
    for (const member of item.members) {
      if (member._id === auth.user._id) {
        myGroup = item;
      }
    }
  }

  const handleSubmit2 = (e) => {
    e.preventDefault();
    dispatch(createPost({ content, auth, myCS, myGroup }));
    setContent("");
  };

  const handleSubmit3 = (e) => {
    e.preventDefault();
    const userID = myCS.teacher._id;
    dispatch(createPost({ content, auth, myCS, userID }));
    setContent("");
  };

  return (
    <>
      {myAuth && (
        <div className="post" onClick={handleClick}>
          <img src={myAuth.avatar} alt="" />
          <Link to="#">Thông báo nội dung nào đó cho lớp học của bạn</Link>
        </div>
      )}
      {open ? (
        <form style={{ padding: "20px " }}>
          <textarea
            className="inputPost"
            name="content"
            value={content}
            placeholder={`Đăng bài thảo luận đi nào, ${myAuth.fullName}...`}
            onChange={(e) => setContent(e.target.value)}
          />

          <div className="d-flex justify-content-center">
            <Button
              onClick={handleSubmit}
              variant="contained"
              style={{ width: "50", margin: "4px" }}
            >
              Đăng với chế độ công khai
            </Button>
            {auth.user.role !== "teacher" && (
              <Button
                onClick={handleSubmit2}
                variant="contained"
                style={{ width: "50", margin: "4px" }}
              >
                Đăng lên nhóm {myGroup ? myGroup.name : ""}
              </Button>
            )}
            {auth.user.role !== "teacher" && (
              <Button
                onClick={handleSubmit3}
                variant="contained"
                style={{ width: "50", margin: "4px" }}
              >
                Gửi yêu cầu và hỏi bài GV {myCS ? myCS.teacher.fullName : ""}
              </Button>
            )}
          </div>
          {/* <button type="submit">Đăng</button> */}
        </form>
      ) : null}
    </>
  );
};

// const Ckeditor = (props) => {
//   const [data, setData] = useState("");

//   const onChange = (event, editor) => {
//     const data = editor.getData();
//     setData(data);
//   };

//   useEffect(() => {});
//   return (
//     <div>
//       <CKEditor
//         editor={ClassicEditor}
//         onChange={onChange}
//         data={data}
//         onReady={(e) => console.log(e)}
//       />
//     </div>
//   );
// };

ClassPost.propTypes = {};

export default ClassPost;
