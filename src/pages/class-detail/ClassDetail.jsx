import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";
import "./class-detail.scss";
import { Box, Container, Grid, Paper, styled } from "@mui/material";
import ClassDetailCard from "./ClassDetailCard";
import ClassDealine from "./ClassDealine";
import ClassPost from "./ClassPost";
import ClassPostList from "./ClassPostList";
import ClassPostAssignment from "./ClassPostAssignment";
import Comment from "./Comment";
// import { getCSListByIdS, getDataCSByIdCS } from "../../assets/js/classSubject";
import RequestChangeGroup from "./RequestChangeGroup/RequestChangeGroup";
import { useSelector, useDispatch } from "react-redux";
import { getPosts } from "../../redux/actions/postAction";
const Item = styled(Paper)(({ theme }) => ({
  backgroundColor: theme.palette.mode === "dark" ? "#1A2027" : "#fff",
  ...theme.typography.body2,
  padding: theme.spacing(1),
  border: "1px solid #dadce0",
  borderRadius: "0.5rem",
  marginBottom: "20px"
}));

const ClassDetail = ({ myRequestGr, myAuth, myCS }) => {
  const { auth, post } = useSelector((state) => state);

  const dispatch = useDispatch();
  useEffect(() => {
    if (auth.token && myCS._id) dispatch(getPosts({ auth, myCS }));
  }, [dispatch, auth.token, myCS]);

  return (
    <>
      {myCS && (
        <Box>
          <Box className="container">
            <ClassDetailCard myCS={myCS} />
            <Box sx={{ flexGrow: 1, marginTop: "20px" }}>
              <Grid container spacing={2}>
                <Grid item xs={3} className="container__left">
                  <Item>
                    <ClassDealine />
                  </Item>
                  <Item>
                    <RequestChangeGroup
                      myRequestGr={myRequestGr}
                      myCS={myCS}
                      groups={myCS.groups}
                    />
                  </Item>
                </Grid>
                <Grid item xs={9} className="container__right">
                  <Item>
                    <ClassPost myAuth={myAuth} myCS={myCS} />
                  </Item>
                  {/* <Item>{myCS && <ClassPostAssignment myCS={myCS} />}</Item> */}
                  {post.posts.map((item) => (
                    <Item
                      key={item._id}
                      sx={{
                        padding: 0,
                        marginBottom: "14px",
                        borderRadius: "10px"
                      }}
                    >
                      <ClassPostList post={item} />
                      <Comment comment={item.comments} post={item} />
                    </Item>
                  ))}
                </Grid>
              </Grid>
            </Box>
          </Box>
        </Box>
      )}
    </>
  );
};

ClassDetail.propTypes = {};

export default ClassDetail;
