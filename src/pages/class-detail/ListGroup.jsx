import React, { useState } from "react";
import PropTypes from "prop-types";
import "./class-detail.scss";
import { BsArrowRightCircle } from "react-icons/bs";
import { Container, IconButton } from "@mui/material";
import avatardefault from "../../assets/img/avatar/avatar-default.png";
import group from "../../Mock/listgroup";
import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";
import Modal from "@mui/material/Modal";
import { Formik, Field, Form } from "formik";
import { GrAdd } from "react-icons/gr";
import SelectChipMember from "./SelectChipMember";
import logo from "../../assets/img/logo/logo.png";
import DeleteIcon from "@mui/icons-material/Delete";
import Group from "./Group";
import { useSelector, useDispatch } from "react-redux";
import { insertGroup, addMembers } from "../../redux/actions/groupAction";

import "react-toastify/dist/ReactToastify.css";

import { toast } from "react-toastify";
// MUI MULTIPLE SELECT
import Checkbox from "@mui/material/Checkbox";
import TextField from "@mui/material/TextField";
import Autocomplete from "@mui/material/Autocomplete";
import CheckBoxOutlineBlankIcon from "@mui/icons-material/CheckBoxOutlineBlank";
import CheckBoxIcon from "@mui/icons-material/CheckBox";

const icon = <CheckBoxOutlineBlankIcon fontSize="small" />;
const checkedIcon = <CheckBoxIcon fontSize="small" />;
toast.configure();

const style = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 800,
  bgcolor: "background.paper",
  border: "1px solid #ccc",
  boxShadow: 24,
  p: 4
};
const ListGroup = ({ myCS }) => {
  const { auth, group } = useSelector((state) => state);
  let arrGr = [];
  for (const item of group.groups) {
    arrGr.push(item.members);
  }
  let arrGr2 = [];
  for (const iterator of arrGr) {
    for (const v2 of iterator) {
      arrGr2.push(v2);
    }
  }

  const A = myCS.students;
  const B = arrGr2;

  // Lọc ra các thành viên chưa có nhóm
  const myFinallyResult = A.filter((a) => !B.map((b) => b._id).includes(a._id));
  const dispatch = useDispatch();

  const [myStudents, setMyStudents] = useState([]);
  const [open, setOpen] = React.useState(false);
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);

  return (
    <>
      <Container maxWidth="lg" className="list-group">
        <div className="list-group__header">
          <h1>Danh sách nhóm</h1>
          <IconButton
            title="Thêm Nhóm"
            size="large"
            className="list-group__header__icon"
            onClick={handleOpen}
          >
            <GrAdd className="list-group__header__icon--add" />
          </IconButton>
        </div>
        <div className="list-group__content">
          {group.groups.map((item, i) => (
            <Group members={myFinallyResult} item={item} key={i} />
          ))}
        </div>
      </Container>
      <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={style}>
          <Typography
            id="modal-modal-title"
            variant="h6"
            component="h2"
            className="modal-title"
          >
            Tạo nhóm
          </Typography>
          <div id="modal-modal-description" sx={{ mt: 2 }}>
            <Formik
              initialValues={{
                namegroup: "",
                member: ""
              }}
              onSubmit={async (values) => {
                const checkName = myCS.groups.filter(
                  (group) => group.name === values.namegroup
                );
                if (checkName.length > 0) {
                  toast.error(`${values.namegroup} đã bị trùng`);
                } else {
                  if (myStudents.length > 0) {
                    dispatch(insertGroup({ auth, values, myCS }));

                    setTimeout(() => {
                      dispatch(addMembers({ auth, values, myStudents }));
                    }, 1000);
                  } else {
                    dispatch(insertGroup({ auth, values, myCS }));
                  }
                  handleClose();
                }
              }}
            >
              <Form className="add-form">
                <div className="add-subform modal-addgroup">
                  <label htmlFor="namegroup" style={{ fontSize: "18px" }}>
                    Tên Nhóm
                  </label>
                  <Field
                    style={{ width: "55%", padding: "18px" }}
                    id="namegroup"
                    name="namegroup"
                    placeholder="Điền tên nhóm"
                  />
                </div>
                <div
                  style={{
                    display: "flex",
                    direction: "row",
                    alignItems: "center",
                    justifyContent: "inherit"
                  }}
                >
                  <label
                    htmlFor="member"
                    style={{
                      textAlign: "left",
                      fontSize: "16px",
                      flexBasis: "20%"
                    }}
                  >
                    Thành viên
                  </label>
                  {/* MULTIPLE SELECT */}

                  {myCS.students.length > 0 && (
                    <Autocomplete
                      multiple
                      id="checkboxes-tags-demo"
                      onChange={(event, value) => setMyStudents(value)}
                      options={myFinallyResult}
                      noOptionsText="Các nhóm đã đủ thành viên...."
                      disableCloseOnSelect
                      getOptionLabel={(option) => option.fullName}
                      renderOption={(props, option, { selected }) => (
                        <li {...props}>
                          <Checkbox
                            icon={icon}
                            checkedIcon={checkedIcon}
                            style={{ marginRight: 8 }}
                            checked={selected}
                          />
                          {option.fullName}
                        </li>
                      )}
                      style={{ width: 400, marginBottom: "30px" }}
                      renderInput={(params) => (
                        <TextField
                          {...params}
                          label="Sinh viên"
                          placeholder="Chọn sinh viên mà bạn muốn thêm vào group"
                        />
                      )}
                    />
                  )}
                  {/* END */}
                </div>
                {/* <div className="add-subform modal-addgroup">
                  <label htmlFor="member">Thành viên</label>
                 <Field
                    as={
                      <SelectChipMember
                        groups={myCS.groups}
                        students={myCS.students}
                      />
                    }
                    id="member"
                    name="member"
                  />
                  <SelectChipMember
                    groups={myCS.groups}
                    students={myCS.students}
                    id="member"
                    name="member"
                  />
                </div> */}
                <div className="add-action">
                  <Button variant="contained" size="large" type="submit">
                    Tạo Nhóm
                  </Button>
                  <Button
                    variant="contained"
                    size="large"
                    color="error"
                    onClick={handleClose}
                  >
                    Hủy
                  </Button>
                </div>
              </Form>
            </Formik>
          </div>
        </Box>
      </Modal>
    </>
  );
};

ListGroup.propTypes = {};

export default ListGroup;
