import React, { useState } from "react";
import { useFormik } from "formik";
import * as yup from "yup";
import { Button, TextField } from "@mui/material";
import Autocomplete from "@mui/material/Autocomplete";
import { useSelector, useDispatch } from "react-redux";
import { createRequestChangeGroup } from "../../../redux/actions/requestChangeGroup";
const validationSchema = yup.object({
  groupcurrent: yup
    .string("Enter your Group Current")
    .required("this is required"),
  groupchange: yup
    .string("Enter your Group want change")
    .required("Password is required"),
  content: yup.string("Enter content change group").required("this is required")
});

const SendChangeGroup = ({ handleCloseModal, myCS, myGroup, groups }) => {
  const { auth } = useSelector((state) => state);
  const dispatch = useDispatch();

  const [groupChange, setGroupChange] = useState("");
  const [reasonChange, setReasonChange] = useState("");

  const groupComfortable = groups.filter((group) => group._id !== myGroup._id);
  const options = groupComfortable.map((option) => {
    const firstLetter = option.name[0].toUpperCase();
    return {
      firstLetter: /[0-9]/.test(firstLetter) ? "Số tăng dần" : firstLetter,
      ...option
    };
  });
  // const kq = groups.filter((group) => group._id !== myGroup._id);
  // console.log("kq", kq);

  const handleSendRequestChangeGroup = () => {
    const title = `Đơn chuyển nhóm từ ${myGroup.name} qua nhóm ${groupChange.name} của ${auth.user.fullName} với lý do: ${reasonChange}`;
    // const content = reasonChange
    // const groupID = myGroup.
    dispatch(
      createRequestChangeGroup({
        title,
        reasonChange,
        myGroup,
        groupChange,
        auth,
        myCS
      })
    );
    if (title && reasonChange && groupChange) {
      handleCloseModal();
    }
  };

  const formik = useFormik({
    initialValues: {
      groupcurrent: "",
      groupchange: "",
      content: ""
    },
    validationSchema: validationSchema,
    onSubmit: (values) => {
      // console.log("shiba");
      // handleSendRequestChangeGroup(values.content);
    }
  });
  return (
    <div className="send-change-group">
      <h1>Nội Dung Gửi Yêu Cầu</h1>
      <form onSubmit={formik.handleSubmit}>
        <TextField
          disabled
          fullWidth
          sx={{ margin: "8px 0" }}
          id="outlined-disabled"
          label={`Nhóm hiện tại`}
          value={myGroup.name}
          // defaultValue={myGroup.name}
        />
        <Autocomplete
          fullWidth
          value={groupChange.name}
          defaultValue={groupChange.name}
          getOptionLabel={(option) => option.name}
          isOptionEqualToValue={(option, value) => option.name === value}
          onChange={(event, newValue) => {
            setGroupChange(newValue);
          }}
          id="grouped-demo"
          sx={{ margin: "8px 0 " }}
          options={options.sort(
            (a, b) => -b.firstLetter.localeCompare(a.firstLetter)
          )}
          groupBy={(option) => option.firstLetter}
          renderInput={(params) => (
            <TextField {...params} label="Nhóm muốn chuyển" />
          )}
        />

        <TextField
          fullWidth
          id="content"
          name="content"
          label="Lý do"
          sx={{ marginTop: "8px" }}
          multiline
          rows={4}
          value={reasonChange}
          // defaultValue={reasonChange}
          onChange={(e) => setReasonChange(e.target.value)}
        />

        <Button
          sx={{ marginTop: "10px" }}
          color="primary"
          variant="contained"
          fullWidth
          type="submit"
          onClick={handleSendRequestChangeGroup}
        >
          Gửi Yêu Cầu
        </Button>
      </form>
    </div>
  );
};

export default SendChangeGroup;
