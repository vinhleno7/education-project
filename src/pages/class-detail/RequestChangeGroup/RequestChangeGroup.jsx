import React, { useState } from "react";
import PropTypes from "prop-types";
import "./request-change-group.scss";
import { Button, Menu, MenuItem, Modal, Typography } from "@mui/material";
import { Box } from "@mui/system";
import SendChangeGroup from "./SendChangeGroup";
import { useSelector, useDispatch } from "react-redux";
import { replyToRequestChangeGroup } from "../../../redux/actions/requestChangeGroup.js";
import moment from "moment";

const style = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 400,
  bgcolor: "#fff",
  borderRadius: "5px",
  boxShadow: 24,
  p: 4
};

const RequestChangeGroup = ({ myRequestGr, myCS, groups }) => {
  const { auth, itemCS } = useSelector((state) => state);
  const dispatch = useDispatch();

  const [myGroup, setMyGroup] = useState("");

  const [anchorEl, setAnchorEl] = useState(null);
  const open = Boolean(anchorEl);
  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };
  const [openModal, setOpenModal] = useState(false);
  const handleOpenModal = () => {
    for (const group of groups) {
      for (const member of group.members) {
        if (member._id === auth.user._id) {
          setMyGroup(group);
          break;
        }
      }
    }

    setOpenModal(true);
    setAnchorEl(null);
  };
  const handleCloseModal = () => setOpenModal(false);

  const [turnModalRequestGr, setTurnModalRequestGr] = useState(false);
  const [myTempRGR, setMyTempRGR] = useState({});

  const handleOpenModalRequestGr = (request) => {
    if (auth.user._id === itemCS.teacher._id) {
      setMyTempRGR(request);
      setTurnModalRequestGr(true);
    }
  };

  const handleAcceptRequestChangeGroup = () => {
    const status = "yes";
    dispatch(replyToRequestChangeGroup({ myTempRGR, status, auth }));
    setTurnModalRequestGr(false);
  };
  const handleDenyRequestChangeGroup = () => {
    const status = "no";
    dispatch(replyToRequestChangeGroup({ myTempRGR, status, auth }));

    setTurnModalRequestGr(false);
  };

  return (
    <>
      <div className="request-change-group">
        <h3>Yêu cầu chuyển nhóm</h3>
        {myRequestGr.result > 0 ? (
          myRequestGr.requestGrs.map((request) => (
            <div
              key={request._id}
              className={
                request.status === "yes"
                  ? "requestGroup status_yes"
                  : request.status === "no"
                  ? "requestGroup status_no"
                  : "requestGroup"
              }
            >
              <span
                // className="requestGroup"
                onClick={() => handleOpenModalRequestGr(request)}
              >
                {request.title}
                <p style={{ textAlign: "right" }}>
                  {" "}
                  {moment(request.createdAt).fromNow()}
                </p>
              </span>
              {myTempRGR.title && (
                <div
                  className={
                    turnModalRequestGr
                      ? "detail_RequestGr turnRequestGr"
                      : "detail_RequestGr"
                  }
                >
                  <h3>{myTempRGR.title.toUpperCase()}</h3>
                  <p>Lý do: {myTempRGR.content}</p>
                  <p>Sinh viên: {myTempRGR.student.fullName} </p>
                  <div className="d-flex justify-content-center">
                    <Button
                      variant="contained"
                      style={{ margin: "4px" }}
                      onClick={() => handleAcceptRequestChangeGroup()}
                    >
                      Đồng ý
                    </Button>
                    <Button
                      variant="contained"
                      color="error"
                      style={{ margin: "4px" }}
                      onClick={() => handleDenyRequestChangeGroup()}
                    >
                      Từ chối
                    </Button>
                    <Button
                      variant="contained"
                      color="error"
                      style={{ margin: "4px" }}
                      onClick={() => setTurnModalRequestGr(false)}
                    >
                      Hủy
                    </Button>
                  </div>
                </div>
              )}
            </div>
          ))
        ) : (
          <span>Không có yêu cầu chuyển nhóm nào</span>
        )}
      </div>
      <div className="request-change-group-btn">
        <Button
          variant="outlined"
          id="basic-button"
          aria-controls={open ? "basic-menu" : undefined}
          aria-haspopup="true"
          aria-expanded={open ? "true" : undefined}
          onClick={handleClick}
        >
          Gửi yêu cầu
        </Button>
      </div>
      <Menu
        id="basic-menu"
        anchorEl={anchorEl}
        open={open}
        onClose={handleClose}
        MenuListProps={{
          "aria-labelledby": "basic-button"
        }}
      >
        <MenuItem
          onClick={handleOpenModal}
          sx={{ fontSize: "1.2rem", fontWeight: "bold" }}
        >
          Xin chuyển nhóm
        </MenuItem>
      </Menu>
      <Modal
        open={openModal}
        onClose={handleCloseModal}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={style}>
          <SendChangeGroup
            handleCloseModal={handleCloseModal}
            myCS={myCS}
            myGroup={myGroup}
            groups={groups}
          />
        </Box>
      </Modal>
    </>
  );
};

RequestChangeGroup.propTypes = {};

export default RequestChangeGroup;
