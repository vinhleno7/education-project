import React from "react";
import { IoMdMore } from "react-icons/io";
import { CgNotes } from "react-icons/cg";
import { IconButton, Menu, MenuItem } from "@mui/material";
import "./class-detail.scss";
import { Link } from "react-router-dom";

const ClassPostAssignment = ({ myCS }) => {
  const [anchorEl, setAnchorEl] = React.useState(null);
  const open = Boolean(anchorEl);
  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };
  return (
    <Link to="/">
      {/* <div className="post-assignment">
        <div className="post-assignment__left">
          <div className="post-assignment__left__note">
            <CgNotes className="post-assignment__left__note--icon" />
          </div>
          <div className="post-assignment__left__content">
            <Link to=" /">
              {myCS.teacher.fullName} đã đăng một bài tập mới: Báo cáo tuần thứ
              2
            </Link>
            <span>27 thg 3</span>
          </div>
        </div>
        <div className="post-assignment__right">
          <IconButton
            onClick={handleClick}
            aria-controls={open ? "account-menu" : undefined}
            aria-haspopup="true"
            aria-expanded={open ? "true" : undefined}
            color="primary"
            size="large"
            aria-label=""
          >
            <IoMdMore className="post-assignment__right__icon" />
          </IconButton>
        </div>
      </div>
      <Menu
        anchorEl={anchorEl}
        id="account-menu"
        open={open}
        onClose={handleClose}
        onClick={handleClose}
        PaperProps={{
          elevation: 0,
          sx: {
            overflow: "visible",
            filter: "drop-shadow(0px 2px 8px rgba(0,0,0,0.32))",
            mt: 1.5,
            "& .MuiAvatar-root": {
              width: 32,
              height: 32,
              ml: -0.5,
              mr: 1,
              boxShadow:
                "rgba(0, 0, 0, 0.16) 0px 10px 36px 0px, rgba(0, 0, 0, 0.06) 0px 0px 0px 1px"
            },
            "&:before": {
              content: '""',
              display: "block",
              position: "absolute",
              top: 0,
              right: 14,
              width: 10,
              height: 10,
              bgcolor: "background.paper",
              transform: "translateY(-50%) rotate(45deg)",

              zIndex: 0
            }
          }
        }}
        transformOrigin={{ horizontal: "right", vertical: "top" }}
        anchorOrigin={{ horizontal: "right", vertical: "bottom" }}
      >
        <MenuItem sx={{ fontSize: "15px" }}>Sao chép đường liên kết</MenuItem>
      </Menu> */}
    </Link>
  );
};

ClassPostAssignment.propTypes = {};

export default ClassPostAssignment;
