import React from "react";
import PropTypes from "prop-types";
import "./class-list.scss";
import { Button, TextField } from "@mui/material";
import * as yup from "yup";
import { useFormik } from "formik";
import { useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { addStudentCS } from "../../redux/actions/classSubjectAction";

const validationSchema = yup.object({
  codeclass: yup
    .string("Enter your Code Class")
    .min(6, "Password should be of minimum 6 characters length")
    .required("Code Class is required")
});

const JoinClass = ({ handleCloseModal }) => {
  const { auth } = useSelector((state) => state);
  const dispatch = useDispatch();
  const formik = useFormik({
    initialValues: {
      codeclass: ""
    },
    validationSchema: validationSchema,
    onSubmit: (values) => {
      const classSubjectID = values.codeclass;
      const studentID = auth.user._id;
      dispatch(addStudentCS({ auth, classSubjectID, studentID }));
    }
  });
  return (
    <div className="joinclass">
      <h1>Mã Lớp</h1>
      <p>Đề nghị giáo viên của bạn cung cấp mã lớp rồi nhập đó vào đây</p>
      <form onSubmit={formik.handleSubmit}>
        <TextField
          fullWidth
          id="codeclass"
          name="codeclass"
          label="Mã Lớp"
          value={formik.values.codeclass}
          onChange={formik.handleChange}
          error={formik.touched.codeclass && Boolean(formik.errors.codeclass)}
          helperText={formik.touched.codeclass && formik.errors.codeclass}
        />
        <Button
          color="primary"
          variant="contained"
          sx={{ marginTop: "20px" }}
          type="submit"
        >
          Tham gia lớp
        </Button>
        <Button
          color="error"
          variant="contained"
          sx={{ marginTop: "20px", marginLeft: "20px" }}
          onClick={handleCloseModal}
        >
          Hủy
        </Button>
      </form>
    </div>
  );
};

export default JoinClass;
