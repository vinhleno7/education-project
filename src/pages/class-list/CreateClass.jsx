import { Button, Typography } from "@mui/material";
import { Field, Form, Formik } from "formik";
import React, { useEffect, useState } from "react";
import "./class-list.scss";
import * as Yup from "yup";
import { toast } from "react-toastify";
import { useDispatch, useSelector } from "react-redux";
import axios from "axios";
import { createClassSubject } from "../../redux/actions/classSubjectAction";
toast.configure();
const CreateClass = ({ handleCloseModalAddCS }) => {
  const { auth } = useSelector((state) => state);
  const [classes, setClasses] = useState([]);
  const valid = Yup.object({
    nameclasssubject: Yup.string().required("No name provided."),
    codesubject: Yup.string().required("This is required!"),
    schoolyear: Yup.string().required("This is required!"),
    note: Yup.string().required("This is required!"),
    teacherid: Yup.string().required("This is required!"),
  });
  const dispatch = useDispatch();
  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await axios.get(
          "http://localhost:5001/api/subject/getAllSubject",
          {
            headers: { Authorization: auth.token },
          }
        );
        setClasses(response.data);
      } catch (error) {
        console.error(error);
      }
    };
    fetchData();
  }, []);
  const handleSubmit = async (values) => {
    const nameclasssubject = values.nameclasssubject;
    const codesubject = values.codesubject;
    const schoolyear = values.schoolyear;
    const note = values.note;
    const teacherid = values.teacherid;
    let dataAddCS = {
      name: nameclasssubject,
      subjectID: codesubject,
      schoolYear: schoolyear,
      note: note,
      roleUser: auth.user.role,
      teacherID: teacherid,
    };
    dispatch(createClassSubject({ auth, dataAddCS }));
    handleCloseModalAddCS();
  };
  return (
    <div>
      <Typography component="h2" className="add-title" variant="h2">
        Nhập thông tin lớp môn học{" "}
      </Typography>
      <Formik
        initialValues={{
          nameclasssubject: "",
          codesubject: "",
          schoolyear: "",
          note: "",
          teacherid: "",
        }}
        validationSchema={valid}
        onSubmit={handleSubmit}
      >
        {({ errors, touched }) => (
          <Form className="add-form">
            <div className="add-subform">
              <label htmlFor="className">Tên Lớp Học</label>
              <Field
                id="nameclasssubject"
                name="nameclasssubject"
                placeholder="English"
              />
            </div>
            {errors.nameclasssubject && touched.nameclasssubject ? (
              <div style={{ color: "red", textAlign: "left" }}>
                {errors.nameclasssubject}
              </div>
            ) : null}

            <div className="add-subform">
              <label htmlFor="codesubject">Môn Học</label>
              <Field as="select" name="codesubject">
                {classes.map((item, index) => (
                  <option value={item._id} key={index}>
                    {item.name}
                  </option>
                ))}
              </Field>
            </div>
            <div className="add-subform">
              <label htmlFor="schoolyear">Năm Học</label>
              <Field
                id="schoolyear"
                name="schoolyear"
                placeholder="2021-2022"
              />
            </div>
            {errors.schoolyear && touched.schoolyear ? (
              <div style={{ color: "red", textAlign: "left" }}>
                {errors.schoolyear}
              </div>
            ) : null}
            <div className="add-subform">
              <label htmlFor="note">Ghi chú</label>
              <Field id="note" name="note" placeholder="Notes" />
            </div>
            {errors.note && touched.note ? (
              <div style={{ color: "red", textAlign: "left" }}>
                {errors.note}
              </div>
            ) : null}
            <div className="add-subform">
              <label htmlFor="note">Mã Giáo viên</label>
              <Field id="teacherid" name="teacherid" />
            </div>
            {errors.teacherid && touched.teacherid ? (
              <div style={{ color: "red", textAlign: "left" }}>
                {errors.teacherid}
              </div>
            ) : null}

            <div className="add-action">
              <Button variant="contained" size="large" type="submit">
                Thêm mới
              </Button>
              <Button
                variant="contained"
                size="large"
                color="error"
                onClick={handleCloseModalAddCS}
              >
                Hủy
              </Button>
            </div>
          </Form>
        )}
      </Formik>
      <span style={{ fontSize: "1.5rem" }}>Mã giáo viên : {auth.user._id}</span>
    </div>
  );
};

export default CreateClass;
