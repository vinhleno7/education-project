import React, { useState } from "react";
import ClassCardItem from "./ClassCardItem";
import {
  Button,
  IconButton,
  Menu,
  MenuItem,
  Modal,
  Typography
} from "@mui/material";
import { Formik, Field, Form } from "formik";
import { GrAdd } from "react-icons/gr";
import Header from "../../components/Header/Header";
import SelectChipMember from "../class-detail/SelectChipMember";
import "./class-list.scss";
// Toast
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { Box } from "@mui/system";
import JoinClass from "./JoinClass";
import { useSelector, useDispatch } from "react-redux";
import CreateClass from "./CreateClass";
toast.configure();
//modal
const style = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: "600px !important",
  bgcolor: "#fff",
  boxShadow: "rgba(0, 0, 0, 0.24) 0px 3px 8px",
  borderRadius: "10px",
  p: 4
};
const ClassList = () => {
  const { auth } = useSelector((state) => state);
  const [anchorEl, setAnchorEl] = useState(null);
  const open = Boolean(anchorEl);
  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };
  //modal
  const [openModal, setOpenModal] = useState(false);
  const handleOpenModal = () => setOpenModal(true);
  const handleCloseModal = () => setOpenModal(false);
  //modal add classSubject
  const [openModalAddCS, setOpenModalAddCS] = useState(false);
  const handleOpenModalAddCS = () => setOpenModalAddCS(true);
  const handleCloseModalAddCS = () => setOpenModalAddCS(false);

  return (
    <>
      <div style={{ margin: "8px 0 0 100px " }}>
        <div className="join-class">
          <h1>Danh sách lớp môn học</h1>
          <div>
            <IconButton
              aria-controls={open ? "account-menu" : undefined}
              aria-haspopup="true"
              aria-expanded={open ? "true" : undefined}
              title="Thêm Nhóm"
              size="large"
              onClick={handleClick}
              className="join-icon"
            >
              <GrAdd className="join-icon--add" />
            </IconButton>
          </div>
        </div>
      </div>
      <div className="classlist">
        <ClassCardItem
          className="classlist__card"
          handleOpenModalAddCS={handleOpenModalAddCS}
        />
      </div>
      <Menu
        anchorEl={anchorEl}
        id="account-menu"
        open={open}
        onClose={handleClose}
        onClick={handleClose}
        PaperProps={{
          elevation: 0,
          sx: {
            overflow: "visible",
            filter: "drop-shadow(0px 2px 8px rgba(0,0,0,0.32))",
            mt: 1.5,
            "& .MuiAvatar-root": {
              width: 32,
              height: 32,
              ml: -0.5,
              mr: 1
            },
            "&:before": {
              content: '""',
              display: "block",
              position: "absolute",
              top: 0,
              right: 14,
              width: 10,
              height: 10,
              bgcolor: "background.paper",
              transform: "translateY(-50%) rotate(45deg)",
              zIndex: 0
            }
          }
        }}
        transformOrigin={{ horizontal: "right", vertical: "top" }}
        anchorOrigin={{ horizontal: "right", vertical: "bottom" }}
      >
        {auth.user.role !== "student" && (
          <MenuItem>
            <Button
              variant="outlined"
              size="large"
              fullWidth
              onClick={handleOpenModalAddCS}
              sx={{ textTransform: "initial", fontSize: "1.5rem" }}
            >
              Tạo lớp học
            </Button>
          </MenuItem>
        )}
        <MenuItem>
          <Button
            variant="outlined"
            size="large"
            fullWidth
            sx={{ textTransform: "initial", fontSize: "1.5rem" }}
            onClick={handleOpenModal}
          >
            Tham gia lớp học
          </Button>
        </MenuItem>
      </Menu>
      <Modal
        open={openModal}
        onClose={handleCloseModal}
        aria-labelledby="parent-modal-title"
        aria-describedby="parent-modal-description"
      >
        <Box sx={{ ...style, width: 400 }}>
          <JoinClass handleCloseModal={handleCloseModal} />
        </Box>
      </Modal>
      <Modal
        open={openModalAddCS}
        onClose={handleCloseModalAddCS}
        aria-labelledby="parent-modal-title"
        aria-describedby="parent-modal-description"
      >
        <Box sx={{ ...style, width: 600 }}>
          <CreateClass handleCloseModalAddCS={handleCloseModalAddCS} />
        </Box>
      </Modal>
    </>
  );
};

export default ClassList;
