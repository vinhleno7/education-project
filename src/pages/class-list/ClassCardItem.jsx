import React, { useState } from "react";
import { Button, IconButton, Menu, MenuItem } from "@mui/material";
import { IoMdMore } from "react-icons/io";
import { FaRegAddressCard } from "react-icons/fa";
import { AiOutlineFolder } from "react-icons/ai";
import "./class-list.scss";
import { useNavigate } from "react-router-dom";
import { Link } from "react-router-dom";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { useSelector, useDispatch } from "react-redux";
// Dialog
import Dialog from "@mui/material/Dialog";
import Slide from "@mui/material/Slide";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogContentText from "@mui/material/DialogContentText";
import DialogTitle from "@mui/material/DialogTitle";
import { TYPES } from "../../redux/actions/authAction";
import backgroundCS from "../.../../../assets/img/bg/p1.jpg";
// import backgroundCS from "../.../../../assets/img/bg/img_backtoschool.jpg";
import { deleteClassSubject } from "../../redux/actions/classSubjectAction";

toast.configure();

const ClassCardItem = ({ handleOpenModalAddCS }) => {
  // Dialog
  const navigate = useNavigate();
  const [openDialog, setOpenDialog] = useState(false);
  const [classSubjects, setClassSubjects] = useState({});
  const handleClickOpenDialog = () => {
    setOpenDialog(true);
  };

  const handleCloseDialog = () => {
    setOpenDialog(false);
  };

  const dialogDelete = () => {
    handleClickOpenDialog();
  };
  const handleDeleteClass = () => {
    handleCloseDialog();
    toast.success(`Đã xóa 1 sinh viên mang tên`);
  };
  const { auth, classSubject } = useSelector((state) => state);
  const dispatch = useDispatch();
  const [anchorEl, setAnchorEl] = React.useState(null);
  const open = Boolean(anchorEl);
  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };

  const idSubLocal = !localStorage.getItem("idSub")
    ? 0
    : localStorage.getItem("idSub");

  const handleGetIdCS = (CS, idCS) => {
    // dispatch
    // dispatch(getDetailCS(auth, idCS));
    navigate(`/detail/${idCS}`);
  };
  const handleClickIdDelete = (id) => {
    dispatch(deleteClassSubject({ id, auth, classSubject }));
  };

  return (
    <>
      {classSubject.result > 0 ? (
        classSubject.classSubjects.map((item, i) => (
          <div className="card" key={i}>
            <div
              className="card__header"
              onClick={() => handleGetIdCS(item, item._id)}
            >
              <div className="card__header__bg">
                <img src={backgroundCS} alt="bg" />
                <div className="card__header__avatar"></div>
              </div>
              <div className="card__header__name">
                <h1
                  onClick={() => handleGetIdCS(item, item._id)}
                  style={{ textAlign: "center" }}
                >
                  {item.name}
                </h1>
                <IconButton
                  onClick={handleClick}
                  aria-controls={open ? "account-menu" : undefined}
                  aria-haspopup="true"
                  aria-expanded={open ? "true" : undefined}
                  color="primary"
                  size="large"
                  aria-label=""
                >
                  <IoMdMore className="card__header__name__icon" />
                </IconButton>
              </div>
              <div className="card__header__name-teacher">
                <span style={{ marginRight: "10px", color: "white" }}>
                  {item.teacher ? item.teacher.fullName : "My Teacher"}
                </span>
              </div>
            </div>
            <div
              className="card__content"
              // onClick={() => handleGetIdCS(item, item._id)}
            >
              <div className="card__content" style={{ cursor: "default" }}>
                {item.subject ? (
                  <p>Môn học: {item.subject.name}</p>
                ) : (
                  <p>Môn học</p>
                )}
                <p>{item.note}</p>
                {auth.user.role === "admin" || auth.user.role === "teacher" ? (
                  <p>Mã lớp môn học : {item._id}</p>
                ) : null}
              </div>
              <div className="card__footer">
                {auth.user.role === "admin" || auth.user.role === "teacher" ? (
                  <div className="card__controls">
                    <Button
                      variant="contained"
                      size="medium"
                      color="success"
                      // onClick={() => handleClickIdUpdate(item._id)}
                      className="card__controls__btn"
                    >
                      Sửa
                    </Button>
                    <Button
                      variant="contained"
                      size="medium"
                      style={{ background: "black" }}
                      className="card__controls__btn"
                      onClick={() => handleClickIdDelete(item._id)}
                    >
                      Xóa
                    </Button>
                  </div>
                ) : null}
                <Dialog
                  open={openDialog}
                  onClose={handleCloseDialog}
                  aria-labelledby="alert-dialog-title"
                  aria-describedby="alert-dialog-description"
                >
                  <DialogTitle id="alert-dialog-title">
                    {"XÓA LỚP MÔN HỌC !"}
                  </DialogTitle>
                  <DialogContent>
                    <DialogContentText id="alert-dialog-description">
                      Bạn có muốn xóa lớp môn học này không ?
                    </DialogContentText>
                  </DialogContent>
                  <DialogActions>
                    <Button onClick={handleCloseDialog}>Hủy</Button>
                    <Button variant="contained" color="error" autoFocus>
                      Xóa
                    </Button>
                  </DialogActions>
                </Dialog>
                <div className="card__footer__icon">
                  <IconButton
                    color="primary"
                    size="large"
                    sx={{ color: "#000" }}
                    title={`Mở bài tập của bạn cho "${item.name}"`}
                  >
                    <FaRegAddressCard />
                  </IconButton>
                  <IconButton
                    color="primary"
                    size="large"
                    title={`Mở thử mục cho "${item.name}" trong Google Drive`}
                    sx={{ color: "#000" }}
                  >
                    <AiOutlineFolder />
                  </IconButton>
                </div>
              </div>
            </div>
          </div>
        ))
      ) : (
        <h1>Bạn không ở trong lớp môn học nào cả.</h1>
      )}
    </>
  );
};

export default ClassCardItem;
