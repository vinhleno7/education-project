import React from "react";
import Header from "../../components/Header/Header";
import Content from "../../components/Content/Content";
function HomePage() {
  return (
    <>
      <Header />
      <Content />
    </>
  );
}

export default HomePage;
