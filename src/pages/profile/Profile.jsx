import { Button, Container } from "@mui/material";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getProfileUsers } from "../../redux/actions/profileAction";
import { useParams } from "react-router-dom";
import EditProfile from "./EditProfile";
import "./profile.scss";
import Header from "../../components/Header/Header";

const Profile = () => {
  const { id } = useParams();
  const { auth, profile } = useSelector((state) => state);
  const [open, setOpen] = useState(false);
  const dispatch = useDispatch();
  const handleClickOpen = () => {
    setOpen(!open);
  };
  useEffect(() => {
    dispatch(getProfileUsers({ users: profile.users, id, auth }));
  }, []);
  const { users } = profile;

  return (
    <>
      <Header />
      <Container maxWidth="lg">
        <div className="profile">
          <div className="profile__left">
            <img src={users.avatar} alt="Ảnh Profile" />
          </div>
          <div className="profile__right">
            <div className="profile__right__top">
              <h1>Mã số sinh viên : {users.studentID}</h1>
            </div>
            <div className="profile__right__center">
              <span>Địa chỉ email:</span> <span>{users.email}</span>
            </div>
            <div className="profile__right__bottom">Chức vụ : {users.role}</div>
            <div className="profile__right__bottom">
              Họ và Tên : {users.fullName}
            </div>
            {users.role === "admin" ? (
              ""
            ) : (
              <>
                <div className="profile__right__bottom">
                  Lớp : {users.class?.name ? users.class?.name : "Cập nhật"}
                </div>
                <div className="profile__right__bottom">
                  Khoa : {users.major?.name ? users.major?.name : "Cập nhật"}
                </div>
              </>
            )}
            <div className="profile__right__bottom">
              Số điện thoại : {users.mobile ? users.mobile : "Cập nhật "}
            </div>
            <div className="profile__right__bottom">
              Giới tính :{" "}
              {users.gender === "male"
                ? "Nam"
                : "Cập nhật" || users.gender === "female"
                ? "Nữ"
                : "Cập nhật" || users.gender === "orther"
                ? "Khác"
                : "Cập nhật"}
            </div>
            <div className="profile__right__bottom">
              Website :{" "}
              <a href=" ">{users.website ? users.website : "Cập nhật"}</a>
            </div>
            <Button variant="outlined" onClick={handleClickOpen}>
              Cập nhật thông tin
            </Button>
          </div>
        </div>
      </Container>
      <div className="profile__updateinfor">
        {open ? <EditProfile handleClickOpen={handleClickOpen} /> : null}
      </div>
    </>
  );
};

export default Profile;
