import React, { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { checkImage } from "../../utils/imageUpload";
import { GLOBALTYPES } from "../../redux/actions/globalTypes";
import { updateProfileUser } from "../../redux/actions/profileAction";
import { AiFillCamera } from "react-icons/ai";
import "./profile.scss";
import { Autocomplete, TextField } from "@mui/material";
import axios from "axios";
const EditProfile = ({ handleClickOpen }) => {
  const initState = {
    fullName: "",
    mobile: "",
    website: "",
    majorID: "",
    password: "",
    classID: "",
    gender: "",
  };
  const [userData, setUserData] = useState(initState);
  const {
    fullName,
    mobile,
    website,
    major,
    password,
    class: classes,
    gender,
  } = userData;

  const [avatar, setAvatar] = useState("");
  const [majors, setMajors] = useState([]);
  const [classGetAll, setClassGetAll] = useState([]);
  const { auth, theme } = useSelector((state) => state);
  const dispatch = useDispatch();

  useEffect(() => {
    setUserData(auth.user);
  }, [auth.user]);
  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await axios.get(
          "http://localhost:5001/api/major/getAll",
          {
            headers: { Authorization: auth.token },
          }
        );
        setMajors(response.data.majors);
      } catch (error) {
        console.error(error);
      }
    };
    fetchData();
  }, []);
  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await axios.get(
          "http://localhost:5001/api/class/getAll",
          {
            headers: { Authorization: auth.token },
          }
        );
        setClassGetAll(response.data.classes);
      } catch (error) {
        console.error(error);
      }
    };
    fetchData();
  }, []);
  const changeAvatar = (e) => {
    const file = e.target.files[0];

    const err = checkImage(file);
    if (err)
      return dispatch({
        type: GLOBALTYPES.ALERT,
        payload: { error: err },
      });

    setAvatar(file);
  };

  const handleInput = (e) => {
    const { name, value } = e.target;
    setUserData({ ...userData, [name]: value });
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    dispatch(updateProfileUser({ userData, avatar, auth }));
  };

  return (
    <div className="edit_profile">
      <form onSubmit={handleSubmit}>
        <div className="info_avatar">
          <img
            src={avatar ? URL.createObjectURL(avatar) : auth.user.avatar}
            alt="avatar"
            style={{ filter: theme ? "invert(1)" : "invert(0)" }}
          />
          <span>
            <AiFillCamera className="info_avatar--icon" />
            <p>Thay đổi</p>
            <input
              type="file"
              name="file"
              id="file_up"
              accept="image/*"
              onChange={changeAvatar}
            />
          </span>
        </div>

        <div className="form-group">
          <label htmlFor="fullName">Họ và Tên</label>
          <div className="position-relative">
            <input
              type="text"
              className="form-control"
              id="fullName"
              name="fullName"
              value={fullName}
              onChange={handleInput}
            />
            <small
              className="text-danger position-absolute"
              style={{
                top: "50%",
                right: "5px",
                transform: "translateY(-50%)",
              }}
            ></small>
          </div>
        </div>

        <div className="form-group">
          <label htmlFor="mobile">Số điện thoại</label>
          <input
            type="text"
            name="mobile"
            value={mobile}
            className="form-control"
            onChange={handleInput}
          />
        </div>
        <div className="form-group">
          <label htmlFor="website">Địa chỉ website (Fb, Ins)</label>
          <input
            type="text"
            name="website"
            value={website}
            className="form-control"
            onChange={handleInput}
          />
        </div>
        {auth.user.role === "admin" ? (
          " "
        ) : (
          <>
            <label htmlFor="Khoa">Khoa</label>
            <div className="input-group-prepend px-0 mb-4">
              <select
                name="majorID"
                id="majorID"
                value={major}
                className="custom-select text-capitalize"
                onChange={handleInput}
              >
                {majors.map((item, index) => (
                  <option value={item._id} key={index}>
                    {item.name}
                  </option>
                ))}
              </select>
            </div>
            <label htmlFor="Lớp">Lớp</label>
            <div className="input-group-prepend px-0 mb-4">
              <select
                name="classID"
                id="classID"
                value={classes}
                className="custom-select text-capitalize"
                onChange={handleInput}
              >
                {classGetAll.map((item, index) => (
                  <option value={item._id} key={index}>
                    {item.name}
                  </option>
                ))}
              </select>
            </div>
          </>
        )}
        <div className="form-group">
          <label htmlFor="Mật Khẩu">Mật Khẩu</label>
          <input
            type="password"
            name="password"
            value={password}
            className="form-control"
            onChange={handleInput}
          />
        </div>
        <label htmlFor="gender">Gender</label>
        <div className="input-group-prepend px-0 mb-4">
          <select
            name="gender"
            id="gender"
            value={gender}
            className="custom-select text-capitalize"
            onChange={handleInput}
            defaultValue="male"
          >
            <option value="male">Male</option>
            <option value="female">Female</option>
            <option value="other">Other</option>
          </select>
        </div>

        <button className="btn btn-info w-50" type="submit">
          Save
        </button>
        <button className="btn btn-danger w-50" onClick={handleClickOpen}>
          Close
        </button>
      </form>
    </div>
  );
};

export default EditProfile;
