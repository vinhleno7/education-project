import { Route, Routes } from "react-router-dom";
import Notify from "./components/Notify/Notify";
import Index from "./pages/class-detail/index";
import ClassList from "./pages/class-list/ClassList";
import Error from "./pages/Error/Error";
import HomePage from "./pages/HomePage/HomePage";
import { Index as Login } from "./pages/Login/Index";
import Messenger from "./pages/Messenger/Messenger";
import Setting from "./pages/Setting/Setting";
import { useSelector, useDispatch } from "react-redux";
import { useEffect } from "react";
import { refreshToken } from "./redux/actions/authAction";
import { getCSs } from "./redux/actions/classSubjectAction";
import Profile from "./pages/profile/Profile";
import Dashboard from "./components/Dashboard/Dashboard";
function App() {
  const { auth } = useSelector((state) => state);
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(refreshToken());
  }, [dispatch]);

  useEffect(() => {
    if (auth.token) dispatch(getCSs(auth.token));
  }, [dispatch, auth.token]);
  return (
    <div>
      <Routes>
        <Route path="/" exact element={auth.token ? <HomePage /> : <Login />} />
        <Route path="class-list" element={<ClassList />} />
        <Route
          path="/detail/:id"
          exact
          element={auth.token ? <Index /> : null}
        />
        <Route path="/profile/:id" element={auth.token ? <Profile /> : null} />
        <Route path="/detail" exact element={<Index />} />
        {auth.token &&
          (auth.user.role === "admin" ? (
            <Route path="/dashboard" exact element={<Dashboard />} />
          ) : null)}
        <Route path="/messenger" exact element={<Messenger />} />
        <Route path="*" exact element={<Error />} />
        <Route path="/setting" exact="true" element={<Setting />} />
      </Routes>
    </div>
  );
}

export default App;
